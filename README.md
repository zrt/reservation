# README


## What is the Reservation Tool

![Main page](doc/main.png)

The tool provides a way to show a table of entires in an extendable way. The
original purpose was to help users to allocate targets for tasks (the targets
are the entries which represents an allocatable unit). For example, the
targets represent machines on which manual tests are executed. A test can took
20-30 minutes. The allocation of these machines between the team members are
handled by the the page. When a user wants to run a test on a machine, he/she
locks it and the page displays the target is locked by the user. When tests are
finished, he/she unlocks the target and the next user waiting can lock the machine.


## Installation
To install you will need PHP 5.6 and MySql. Download the to your server.

Execute 'npm install' in the frontend directory. This will install the neccessery
npm packages, the bower components and execute grunt target to create the dist/
directory in the project root. This is the simplified, released version of the ui.

Then execute 'composer install' in the backend directory. It will download the
neccessery php packages.

After these steps, create the alias on your webserver to dist/www directory.

Create a mysql schema with name 'reservation' and then call the dist/www/api/install.php
which creates the tables.

## Documentation

### Backend
The backend is written in PHP using Silex and Symfony. It provides API to handle
targets and its meta parameters. The data for targets are key/value pairs associated
to a given target id.

#### Keys & values
The keys are basically the 'columns' for the given target. The keys are arbitrary
strings and does not have to be pre-defined.
The values are strings. Their interpretations can be changed with meta parameters.
The currently supported value types are: value and json.

#### Meta parameters
Metas are a bunch of key/value pairs associated to a given key. They provide
ways to change behaviour. The metas are used by both the UI and the backend.
The keys and values are also arbitrary strings, the system does and (should)
not require  a existence of a key. One can store configuration keys for an
external tool in the meta parameters as they can be queried.

The currently supported meta parameters for targets:

 * filter: yes/no.
    yes indicates that filtering option is enabled in the column header
 * order: integer id for column ordering
 * name: string to display as column header
 * zones.view: (empty)/'user'/a username:
    * if empty or not provided, the column is always visible.
    * if 'user' is set, the column is visible only if a user is logged in
    * if a username is set, the column is visible only when the given user
      is logged in.
    When a view rule is set, it does not effect only the visibility of a column
    on the UI, it affects the returned keys of a GET query too.
 *  display: the type how to display the key. Currently supported:
    * value: displaying as is
    * lock: a custom directive in the UI which provides a way to
      lock/unlock and queue up for users
 * type: value/json
    * the 'value' value type indicates that no transformation is done on the
    value before/after an update/query.
    * the json type indicates that prior to a key update, the value is json
    encoded and for a query the value is returned as json decoded. This just
    means that when a column is defined as json, a json array can be put under
    the key, and at query the json object is returned (for example a json array).

#### Targets
Targets are the row of the displayed table. A target entry can contain lots of
key/value pairs. Those keys are displayed as a column only which has a 'display' meta
parameter assigned.

##### End points
* GET /api/target
* GET /api/target/{id}
* DELETE /api/target/{id}
* PUT /api/target/{id}
* POST /api/target

#### Security
For some restricted operations a token is required in the Authorization header.
The token should be retrieved by a login operation but it is not implemented yet.
Instead the token is a simple string containing the username the user entered.

##### Admin
Logging in with 'admin' username can provide ability to edit a target or to
access the settings page where meta and raw target parameters can be edited.


## Development

### PHP Unittest
Part of the unittests are tested against a mysql server. To execute them
you need to create an empty 'reservation_test' database. The test will create
the neccessary database structures for itself.