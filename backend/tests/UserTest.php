<?php

namespace User;

class UserTest extends \PHPUnit_Framework_TestCase {

    /**
     * @expectedException InvalidArgumentException
     */
    public function testLoad_throwsException_whenEmptyUsername() {
        $user = new User();
        $user->load("");
    }

    public function testGetUsername() {
        $user = new User();
        $user->load("user1name");

        $this->assertEquals("user1name", $user->getUsername());
    }

    public function testHasZone_whenNoUser() {
        $user = new User();

        $this->assertFalse($user->hasZone("user"));
        $this->assertFalse($user->hasZone("admin"));
    }


    public function testHasZone_whenUser() {
        $user = new User();
        $user->load("bela");

        $this->assertTrue($user->hasZone("user"));
        $this->assertTrue($user->hasZone("bela"));
        $this->assertFalse($user->hasZone("admin"));
    }

    public function testHasZone_whenAdmin() {
        $user = new User();
        $user->load("admin");

        $this->assertTrue($user->hasZone("user"));
        $this->assertTrue($user->hasZone("bela"));
        $this->assertTrue($user->hasZone("admin"));
    }
}

?>