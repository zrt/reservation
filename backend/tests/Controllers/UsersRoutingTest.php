<?php

    namespace Controllers;

    use Silex\WebTestCase;

    class UsersRoutingTest extends WebTestCase
    {
        private $controllerMock;

        public function createApplication()
        {
            $app_test = true;
            $app = require __DIR__.'/../../src/index.php';
            $app['debug'] = true;
            unset($app['exception_handler']);
            $this->controllerMock = $this->getMockBuilder('Controllers\UsersController')
                    ->disableOriginalConstructor()
                    ->getMock();
            $app['UsersController'] = $this->controllerMock;
            return $app;
        }


        public function testGet()
        {
            $this->controllerMock->method('get')->with($this->equalTo('user1'))->will($this->returnValue(array("username" => "user1")));
            $client = $this->createClient();

            $client->request('GET', '/users/user1');
            $this->assertTrue($client->getResponse()->isOk());
        }


        public function testPut()
        {
            $this->controllerMock
                ->method('put')
                ->with()
                ->will($this->returnValue(array()));
            $client = $this->createClient();

            $client->request('PUT', '/users/user2');

            $this->assertTrue($client->getResponse()->isOk());
        }

    }

?>