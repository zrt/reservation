<?php

    namespace Controllers;

    class UsersControllerTest extends \PHPUnit_Framework_TestCase
    {
        protected $controller;
        protected $usersMock;

        public function setUp() {
            $app = new \Silex\Application();
            $this->usersMock = $this->getMockBuilder('Entities\AccessControlledEntities')->disableOriginalConstructor()->getMock();
            $app['users'] = $app->share(function() { return $this->usersMock; });

            $this->controller = new UsersController($app);
        }


        public function testGet_returnsUserinfo_whenOk() {
            $this->usersMock->method('getBy')->with($this->equalTo('username'), $this->equalTo('adm'))->will($this->returnValue(array('id' => '1', 'username' => 'adm', 'email' => 'adm@ad.m')));

            $result = $this->controller->get("adm");

            $this->assertEquals(array('username' => 'adm', 'email' => 'adm@ad.m'), $result);
            $this->assertArrayNotHasKey('id', $result);
        }


        public function testPut_putsUserInfo_whenOk() {
            $request = new \Symfony\Component\HttpFoundation\Request();
            $request->request->replace(json_decode('{"email": "abc"}', true));

            $this->usersMock->method('getBy')
                ->with($this->equalTo('username'), $this->equalTo('adm'))
                ->will($this->returnValue(array('id' => '1', 'username' => 'adm')));

            $this->usersMock
                ->expects($this->once())
                ->method('put')
                ->with($this->equalTo('1'), $this->equalTo(array('email' => 'abc', 'username' => 'adm')));

            $this->controller->put("adm", $request);
        }


        public function testPut_addsUser_whenNotFound() {
            $request = new \Symfony\Component\HttpFoundation\Request();
            $request->request->replace(json_decode('{"email": "abc"}', true));

            $this->usersMock->method('getBy')
                ->with($this->equalTo('username'), $this->equalTo('adm'))
                ->will($this->throwException(new \Entities\NotFoundException("generated")));

            $this->usersMock
                ->expects($this->once())
                ->method('post')
                ->with($this->equalTo(array('email' => 'abc', 'username' => 'adm')));

            $this->controller->put("adm", $request);
        }


    }
?>