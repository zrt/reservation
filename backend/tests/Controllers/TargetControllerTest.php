<?php

    namespace Controllers;

    class UserMock {
        public $username;
        public function getUsername() { return $this->username; }
    }

    class TargetControllerTest extends \PHPUnit_Framework_TestCase
    {
        protected $controller;
        protected $userMock;
        protected $usersMock;
        protected $targetsMock;
        protected $emailMock;

        public function setUp() {
            $app = new \Silex\Application();
            $this->userMock = $app['user'] = new UserMock();
            $this->targetsMock = $this->getMockBuilder('Entities\AccessControlledEntities')->disableOriginalConstructor()->getMock();
            $this->usersMock = $this->getMockBuilder('\Entities\AccessControlledEntities')->disableOriginalConstructor()->getMock();
            $this->emailMock = $this->getMockBuilder('\Email\Email')->disableOriginalConstructor()->getMock();
            $app['targets'] = $app->share(function() { return $this->targetsMock; });
            $app['email'] = $app->share(function() { return $this->emailMock; });
            $app['users'] = $app->share(function() { return $this->usersMock; });

            $this->controller = new TargetController($app);
        }

        /**
         * @expectedException Controllers\UnauthorizedException
         */
        public function testLock_throwsUnauthorized() {
            $this->userMock->username = "";

            $this->controller->lock(1);
        }


        public function testLock_createsLockedField_whenNoLockedFieldYet() {
            $this->userMock->username = "bela";
            $this->targetsMock->method('get')->with($this->equalTo(1))->will($this->returnValue(array()));
            $this->targetsMock->expects($this->once())->method('put')->with($this->equalTo(1), $this->callback(function($subject) { return $subject['locked'] == array('bela'); }));

            $result = $this->controller->lock(1);

            $this->assertEquals($result, array('status' => 'ok'));
        }


        public function testLock_emptiesLockedArray_whenIsNotArray() {
            $this->userMock->username = "bela";
            $this->targetsMock->method('get')->with($this->equalTo(1))->will($this->returnValue(array('locked' => 'bela1')));
            $this->targetsMock->expects($this->once())->method('put')->with($this->equalTo(1), $this->callback(function($subject) { return $subject['locked'] == array('bela'); }));

            $this->controller->lock(1);
        }


        public function testLock_doesNothing_whenAlreadyInQueue() {
            $this->userMock->username = "bela";
            $this->targetsMock->method('get')->with($this->equalTo(1))->will($this->returnValue(array('locked' => array('bela1', 'bela'))));
            $this->targetsMock->expects($this->never())->method('put')->with($this->equalTo(1));

            $result = $this->controller->lock(1);

            $this->assertEquals($result, array('status' => 'ok'));
        }


        public function testLock_addsUserNameToEndOfTheQueue_whenThereIsAList() {
            $this->userMock->username = "bela";
            $this->targetsMock->method('get')->with($this->equalTo(1))->will($this->returnValue(array('locked' => array('bela1', 'bela2'))));
            $this->targetsMock->expects($this->once())->method('put')->with($this->equalTo(1), $this->callback(function($subject) { return $subject['locked'] == array('bela1','bela2','bela'); }));

            $result = $this->controller->lock(1);

            $this->assertEquals($result, array('status' => 'ok'));
        }


        /**
         * @expectedException Controllers\UnauthorizedException
         */
         public function testUnlock_throwsUnauthorized() {
            $this->userMock->username = "";

            $this->controller->unlock(1);
        }


        public function testUnlock_whenUserIsFirst() {
            $this->userMock->username = "bela";
            $this->targetsMock->method('get')->with($this->equalTo(1))->will($this->returnValue(array('locked' => array('bela', 'bela2', 'bela3'))));
            $this->targetsMock->expects($this->once())->method('put')->with($this->equalTo(1), $this->callback(function($subject) { return $subject['locked'] == array('bela2', 'bela3'); }));

            $result = $this->controller->unlock(1);

            $this->assertEquals($result, array('status' => 'ok'));
        }


        public function testUnlock_doesNothing_whenNotLockedByUser() {
            $this->userMock->username = "bela";
            $this->targetsMock->method('get')->with($this->equalTo(1))->will($this->returnValue(array('locked' => array('bela2', 'bela3'))));
            $this->targetsMock->expects($this->never())->method('put')->with($this->equalTo(1));

            $result = $this->controller->unlock(1);

            $this->assertEquals($result, array('status' => 'ok'));
        }


        public function testUnlock_removesMultipleLockForAUser_AndCaseInsensitive() {
            $this->userMock->username = "bela";
            $this->targetsMock->method('get')->with($this->equalTo(1))->will($this->returnValue(array('locked' => array('bela', 'bela2', 'bela', 'bela3', 'Bela', 'bEla', 'bela'))));
            $this->targetsMock->expects($this->once())->method('put')->with($this->equalTo(1), $this->callback(function($subject) { return $subject['locked'] == array('bela2','bela3');}));

            $result = $this->controller->unlock(1);

            $this->assertEquals($result, array('status' => 'ok'));
        }


        public function testUnlock_doesNothing_whenLockedKeyNotExists() {
            $this->userMock->username = "bela";
            $this->targetsMock->method('get')->with($this->equalTo(1))->will($this->returnValue(array()));
            $this->targetsMock->expects($this->never())->method('put')->with($this->equalTo(1));

            $result = $this->controller->unlock(1);

            $this->assertEquals($result, array('status' => 'ok'));
        }


        public function testUnlock_resetsLockField_whenIsNotArray() {
            $this->userMock->username = "bela";
            $this->targetsMock->method('get')->with($this->equalTo(1))->will($this->returnValue(array('locked' => 'bela1')));
            $this->targetsMock->expects($this->once())->method('put')->with($this->equalTo(1), $this->callback(function($subject) { return $subject['locked'] == array(); }));

            $this->controller->unlock(1);
        }


        public function testGetAll_returnsList() {
            $targets = array(
                array('id' => 8, 'name' => 'egy'),
                array('id' => 9, 'name' => 'keto') );
            $this->targetsMock->method('getAll')->will($this->returnValue($targets));
            $this->targetsMock->method('getMetaParameters')->will($this->returnValue(new \Entities\Meta(array('meta'))));

            $result = $this->controller->getAll();

            $this->assertEquals($result, array('entries' => $targets, 'meta' => array('meta')));
        }


        public function testMetaKeyDelete_nominal() {
            $this->targetsMock->expects($this->once())->method('metaKeyDelete')->with($this->equalTo('abc'));

            $result = $this->controller->metaKeyDelete('abc');

            $this->assertEquals($result, array('status' => 'ok'));
        }


        public function testMetaKeyRename_nominal() {
            $this->targetsMock->expects($this->once())->method('metaKeyRename')->with($this->equalTo('abc'), $this->equalTo('def'));
            $request = new \Symfony\Component\HttpFoundation\Request();
            $request->request->replace(json_decode('{"to": "def"}', true));

            $result = $this->controller->metaKeyRename('abc', $request);

            $this->assertEquals($result, array('status' => 'ok'));
        }

        /**
         * @expectedException InvalidArgumentException
         */
        public function testMetaKeyRename_error() {
            $this->targetsMock->expects($this->never())->method('metaKeyRename');
            $request = new \Symfony\Component\HttpFoundation\Request();

            $result = $this->controller->metaKeyRename('abc', $request);
        }


        public function testMetaDelete_nominal() {
            $this->targetsMock->expects($this->once())->method('metaDelete')->with($this->equalTo('abc'), $this->equalTo('def'));

            $result = $this->controller->metaDelete('abc', 'def');

            $this->assertEquals($result, array('status' => 'ok'));
        }


        public function testMetaPost_nominal() {
            $this->targetsMock->expects($this->once())->method('metaPost')->with($this->equalTo('abc'), $this->equalTo(array('key1'=>'value1', 'key2'=>'value2')));
            $request = new \Symfony\Component\HttpFoundation\Request();
            $request->request->replace(json_decode('{"key1": "value1", "key2": "value2"}', true));

            $result = $this->controller->metaPost('abc', $request);

            $this->assertEquals($result, array('status' => 'ok'));
        }


        public function testUnlock_sendsEmailForNextUser_whenUserHasEmailAddress() {
            $target = array('locked' => array('user1', 'user2'));
            $nextUser = array('email' => 'user2@email.c');
            $this->userMock->username = 'user1';
            $this->targetsMock
                    ->method('get')
                    ->with($this->equalTo(1))
                    ->will($this->returnValue($target));

            $this->usersMock
                    ->expects($this->once())
                    ->method('getBy')
                    ->with('username', 'user2')
                    ->will($this->returnValue($nextUser));

            $this->emailMock
                    ->expects($this->once())
                    ->method('send')
                    ->with('unlock', array( 'user' => $nextUser, 'target' => $target), 'user2@email.c');

            $result = $this->controller->unlock(1);
        }


        public function testUnlock_sendsEmailOnlyForNextUser_whenMultipleUsersInQueue() {
            $target = array('locked' => array('user-a', 'user-b', 'user-c'));
            $nextUser = array('email' => 'userb@email.c');
            $this->userMock->username = 'user-a';
            $this->targetsMock
                    ->method('get')
                    ->with(1)
                    ->will($this->returnValue($target));

            $this->usersMock
                    ->expects($this->once())
                    ->method('getBy')
                    ->with('username', 'user-b')
                    ->will($this->returnValue($nextUser));

            $this->emailMock
                    ->expects($this->once())
                    ->method('send')
                    ->with($this->equalTo('unlock'), array( 'user' => $nextUser, 'target' => $target), $this->equalTo('userb@email.c'));

            $result = $this->controller->unlock(1);
        }


        public function testUnlock_doesNotSendEmail_whenListBecomesEmpty() {
            $this->userMock->username = 'user1';
            $this->targetsMock->method('get')->with($this->equalTo(1))->will($this->returnValue(array('locked' => array('user1'))));
            $this->emailMock
                    ->expects($this->never())
                    ->method('send');

            $result = $this->controller->unlock(1);
        }


        public function testUnlock_doesNotSendEmail_whenUserHasNoAssociatedEmailAddress() {
            $this->userMock->username = 'usera';
            $this->targetsMock->method('get')->with($this->equalTo(1))->will($this->returnValue(array('locked' => array('usera', 'userb'))));

            $this->usersMock
                    ->expects($this->once())
                    ->method('getBy')
                    ->with('username', 'userb')
                    ->will($this->returnValue(array()));

            $this->emailMock
                    ->expects($this->never())
                    ->method('send');

            $result = $this->controller->unlock(1);
        }


        public function testUnlock_doesNotSendEmail_whenUserQueryFails() {
            $this->userMock->username = 'usera';
            $this->targetsMock->method('get')->with($this->equalTo(1))->will($this->returnValue(array('locked' => array('usera', 'userb'))));

            $this->usersMock
                    ->expects($this->once())
                    ->method('getBy')
                    ->with('username', 'userb')
                    ->will($this->throwException(new \Entities\NotFoundException('userb')));

            $this->emailMock
                    ->expects($this->never())
                    ->method('send');

            $result = $this->controller->unlock(1);
        }
    }
?>