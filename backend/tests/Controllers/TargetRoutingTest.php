<?php

    namespace Controllers;

    use Silex\WebTestCase;

    class TargetRoutingTest extends WebTestCase
    {
        private $controllerMock;

        public function createApplication()
        {
            $app_test = true;
            $app = require __DIR__.'/../../src/index.php';
            $app['debug'] = true;
            unset($app['exception_handler']);
            $this->controllerMock = $this->getMockBuilder('Controllers\TargetController')
                    ->disableOriginalConstructor()
                    ->getMock();
            $app['targetController'] = $this->controllerMock;
            return $app;
        }


        public function testGetAll()
        {
            $this->controllerMock->method('getAll')->will($this->returnValue(array()));
            $client = $this->createClient();

            $client->request('GET', '/targets/');

            $this->assertTrue($client->getResponse()->isOk());
        }


        public function testGet()
        {
            $this->controllerMock
                ->method('getById')
                ->with($this->equalTo(2))
                ->will($this->returnValue(array()));
            $client = $this->createClient();
            $client->request('GET', '/targets/2');

            $this->assertTrue($client->getResponse()->isOk());
        }


        public function testPost()
        {
            $this->controllerMock
                ->method('post')
                ->with()
                ->will($this->returnValue(array()));

            $client = $this->createClient();
            $client->request('POST', '/targets/');

            $this->assertTrue($client->getResponse()->isOk());
        }


        public function testPut()
        {
            $this->controllerMock
                ->method('put')
                ->with($this->equalTo(2))
                ->will($this->returnValue(array()));

            $client = $this->createClient();
            $client->request('PUT', '/targets/2');

            $this->assertTrue($client->getResponse()->isOk());
        }


        public function testDelete()
        {
            $this->controllerMock
                ->method('delete')
                ->with($this->equalTo(3))
                ->will($this->returnValue(array()));

            $client = $this->createClient();
            $client->request('DELETE', '/targets/3');

            $this->assertTrue($client->getResponse()->isOk());
        }


        public function testLock()
        {
            $this->controllerMock
                ->method('lock')
                ->with($this->equalTo(3))
                ->will($this->returnValue(array()));

            $client = $this->createClient();
            $client->request('POST', '/targets/3/lock');

            $this->assertTrue($client->getResponse()->isOk());
        }


        public function testUnlock()
        {
            $this->controllerMock
                ->method('unlock')
                ->with($this->equalTo(3))
                ->will($this->returnValue(array()));

            $client = $this->createClient();
            $client->request('POST', '/targets/3/unlock');

            $this->assertTrue($client->getResponse()->isOk());
        }

        public function testException() {
            $client = $this->createClient();
            $client->request('GET', '/non-existent');

            $content = json_decode($client->getResponse()->getContent(), true);

            $this->assertTrue(array_key_exists('message', $content['exception']));
            $this->assertEquals(404, $client->getResponse()->getStatusCode());
        }

        public function testMetaDeleteKey()
        {
            $this->controllerMock
                ->method('metaKeyDelete')
                ->with($this->equalTo('abc'))
                ->will($this->returnValue(array()));

            $client = $this->createClient();
            $client->request('DELETE', '/targets/meta/abc');

            $this->assertTrue($client->getResponse()->isOk());
        }

        public function testMetaRenameKey()
        {
            $this->controllerMock
                ->method('metaKeyRename')
                ->with($this->equalTo('abc'))
                ->will($this->returnValue(array()));

            $client = $this->createClient();
            $client->request('PATCH', '/targets/meta/abc', array("{to: 'def'}"));

            $this->assertTrue($client->getResponse()->isOk());
        }

        public function testMetaDelete()
        {
            $this->controllerMock
                ->method('metaDelete')
                ->with($this->equalTo('abc'), $this->equalTo('def'))
                ->will($this->returnValue(array()));

            $client = $this->createClient();
            $client->request('DELETE', '/targets/meta/abc/def');

            $this->assertTrue($client->getResponse()->isOk());
        }

        public function testMetaPost()
        {
            $this->controllerMock
                ->method('metaPost')
                ->with($this->equalTo('abc'))
                ->will($this->returnValue(array()));

            $client = $this->createClient();
            $client->request('POST', '/targets/meta/abc');

            $this->assertTrue($client->getResponse()->isOk());
        }

    }

?>