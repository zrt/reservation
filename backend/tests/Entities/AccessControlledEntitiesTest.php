<?php


class UserMock extends \User\User {
    public $username;
    public function getUsername() { return $this->username; }
}

class AccessControlledEntityTest extends Tests\Entities\BaseTestCase {

    protected $entities;
    protected $userMock;

    public function setUp() {
        parent::setup();

        $this->userMock = new UserMock();
        $this->entities = new Entities\AccessControlledEntities("entity", $this->pdo(), $this->userMock);
        $configuration = new Entities\ConfigurationManagement("entity", $this->pdo());
        $configuration->dropTable();
        $configuration->createTable();
    }


    public function testGet_columnNotReturned_whenViewZoneConditionNotMet() {
        $this->userMock->username = "admin";
        $this->addMeta('entity', 'col1', 'zones.view', 'user');
        $this->addMeta('entity', 'col2', 'zones.view', 'admin');
        $id = $this->entities->post(array(
            'col1' => '1',  // visible     <- user access
            'col2' => '2',  // not-visible <- admin access
            'col3' => '3',  // visible     <- not defined. anybody can access
        ));

        $this->userMock->username = "user";
        $entity = $this->entities->get($id);

        $this->assertEquals(array('id'=>$id, 'col1'=>'1', 'col3'=>'3'), $entity);
    }


    public function testGet_returnsOnlyId_whenAllRequiredFieldsAreRestricted() {
        $this->userMock->username = "admin";
        $this->addMeta('entity', 'col1', 'zones.view', 'user');
        $this->addMeta('entity', 'col2', 'zones.view', 'admin');
        $id = $this->entities->post(array( 'col1' => '1', 'col2' => '2', 'col3'=>'3' ));

        $this->userMock->username = "";
        $entity = $this->entities->get($id, array('col1', 'col2'));

        $this->assertEquals(array('id'=>$id), $entity);
    }

    public function testGet_columnNotReturned_whenViewZoneConditionNotMetAndFieldsRequired() {
        $this->userMock->username = "bela";
        $this->addMeta('entity', 'col1', 'zones.view', 'user');
        $id = $this->entities->post(array(
            'col1' => '1',  // not visible <- dont have user access
            'col2' => '2',  // not-visible <- not queried
        ));

        $this->userMock->username = "";
        $entity = $this->entities->get($id, array('col3', 'col1'));

        $this->assertEquals(array('id'=>$id), $entity);
    }

    public function testGet_onlyIdReturned_whenQueriedColumnNotExists() {
        $id = $this->entities->post(array('col1' => '1'));

        $entity = $this->entities->get($id, array('col2'));

        $this->assertEquals(array('id'=>$id), $entity);
    }

    public function testGetBy_columnNotReturned_whenViewZoneConditionNotMet() {
        $this->userMock->username = "admin";
        $this->addMeta('entity', 'col1', 'zones.view', 'user');
        $this->addMeta('entity', 'col2', 'zones.view', 'admin');
        $id = $this->entities->post(array(
            'col1' => '1',  // visible     <- user access
            'col2' => '2',  // not-visible <- admin access
            'col3' => '3',  // visible     <- not defined. anybody can access
        ));

        $this->userMock->username = "user";
        $entity = $this->entities->getBy('col3', '3');

        $this->assertEquals(array('id'=>$id, 'col1'=>'1', 'col3'=>'3'), $entity);
    }

    /**
     * @expectedException Entities\ForbiddenException
     */
    public function testGetBy_throwsForbiddenException_whenQueryFieldIsRestricted() {
        $this->userMock->username = "admin";
        $this->addMeta('entity', 'col1', 'zones.view', 'user');
        $this->addMeta('entity', 'col2', 'zones.view', 'admin');
        $id = $this->entities->post(array(
            'col1' => '1',  // visible     <- user access
            'col2' => '2',  // not-visible <- admin access
            'col3' => '3',  // visible     <- not defined. anybody can access
        ));

        $this->userMock->username = "user";
        $entity = $this->entities->getBy('col2', '2');
    }

    public function testGetMeta_returnsViewAllowedMetas() {
        $this->userMock->username = "bela";
        $this->addMeta('entity', 'name1', 'prop1', 'value1');
        $this->addMeta('entity', 'name1', 'prop2', 'value2');
        $this->addMeta('entity', 'name2', 'prop1', 'value3');
        $this->addMeta('entity', 'name2', 'zones.view', 'user');
        $this->addMeta('entity', 'name3', 'zones.view', 'admin');

        $meta = $this->entities->getMetaParameters();

        $this->assertEquals(array('name1' => array(             // no restriction
                                        'prop1' => 'value1',
                                        'prop2' => 'value2'),
                                  'name2' => array(             // user zone met
                                        'prop1'=>'value3',
                                        'zones.view' => 'user')),
                                  // missing name3
                            $meta->getValues());
    }


    public function testGetMeta_returnsEmpty() {
        $meta = $this->entities->getMetaParameters();

        $this->assertEquals($meta->getValues(), array());
    }


    public function testGetMeta_returnsEmpty_whenAllRestricted() {
        $this->addMeta('entity', 'name1', 'zones.view', 'user');
        $this->addMeta('entity', 'name2', 'zones.view', 'admin');

        $meta = $this->entities->getMetaParameters();

        $this->assertEquals(array(), $meta->getValues());
    }


    public function testGetAll_returnsArrayOfEntity_withRestrictedColumnsFilteredOut() {
        $this->userMock->username = "bela";
        $this->addMeta('entity', 'a', 'zones.view', 'user');
        $id1 = $this->entities->post(array('a' => '1', 'c' => 'cv'));
        $id2 = $this->entities->post(array('a' => 'b', 'c' => 'd'));
        $id3 = $this->entities->post(array('a' => '2'));
        $id4 = $this->entities->post(array('a' => '3'));

        $this->userMock->username = "";
        $entities = $this->entities->getAll();

        $this->assertEquals(count($entities), 4);
        $this->assertEquals(array('id'=>$id1, 'c'=>'cv'), $entities[0]);
        $this->assertEquals(array('id'=>$id2, 'c'=>'d'), $entities[1]);
        $this->assertEquals(array('id'=>$id3), $entities[2]);
        $this->assertEquals(array('id'=>$id4), $entities[3]);
    }


    public function testPost_ok_whenNoRestrictedFieldsPresent() {
        $this->addMeta('entity', 'param1', 'zones.view', 'user');

        $id = $this->entities->post(array('b' => '1', 'p' => 'cv'));

        $this->assertEquals(array('id'=>$id, 'b'=>'1', 'p'=>'cv'), $this->entities->get($id), 'Entity should be created');
    }


    public function testPost_throwsForbiddenAndItemNotCreated_whenRestrictedFieldPresent() {
        $this->addMeta('entity', 'param1', 'zones.view', 'user');
        try {
            $this->entities->post(array('b' => '1', 'param1' => 'cv'));
            $this->fail("Exception should have been thrown");
        } catch(Entities\ForbiddenException $ex) { /* intentional */ }

        $this->assertEquals(0, count($this->entities->getAll()), 'Entity should not have been added');
    }


    public function testPut_ok_whenNoRestrictedFieldsPresent() {
        $this->userMock->username = "bela";
        $this->addMeta('entity', 'p1', 'zones.view', 'user');
        $id = $this->entities->post(array('b' => '1', 'p1' => 'cv'));

        $this->entities->put($id, array('b'=>'2', 'p1'=>'cv2'));

        $this->assertEquals(array('id'=>$id, 'b'=>'2', 'p1'=>'cv2'), $this->entities->get($id), 'Entity should be updated');
    }


    public function testPut_throwsForbiddenException_whenRestrictedFieldsPresent() {
        $this->userMock->username = "bela";
        $this->addMeta('entity', 'p1', 'zones.view', 'user');
        $id = $this->entities->post(array('b' => '1', 'p1' => 'cv'));

        $this->userMock->username = "";
        try {
            $this->entities->put($id, array('b'=>'2', 'p1'=>'cv2'));
            $this->fail("ForbiddenException should have been thrown");
        } catch(\Entities\ForbiddenException $ex) { /* intentional */ }

        $this->userMock->username = "bela";
        $this->assertEquals(array('id'=>$id, 'b'=>'1', 'p1'=>'cv'), $this->entities->get($id), 'Entity should not be updated');
    }


    public function testDelete_deletesRecord_whenNoRestrictedFieldsPresent() {
        $id = $this->entities->post(array('b' => '1', 'param1' => 'cv'));
        $this->entities->delete($id);

        $this->assertEquals(0, count($this->entities->getAll()), 'Entity should have been removed');
    }


    public function testDelete_throwsException_whenThereAreRestrictedFieldDefinition() {
        $this->userMock->username = "bela";
        $this->addMeta('entity', 'p1', 'zones.view', 'user');
        $id = $this->entities->post(array('b' => '1', 'param1' => 'cv'));

        $this->userMock->username = "";
        try {
            $this->entities->delete($id);
            $this->fail("ForbiddenException should have been thrown");
        } catch(\Entities\ForbiddenException $ex) { /* intentional */ }

        $this->userMock->username = "bela";
        $this->assertEquals(array('id'=>$id, 'b' => '1', 'param1' => 'cv'), $this->entities->get($id), 'Entity should not be removed');
    }
}


?>