<?php


class EntitiesDeleteTest extends Tests\Entities\BaseTestCase {

    public function setUp() {
        parent::setup();
        $configuration = new Entities\ConfigurationManagement("entity", $this->pdo());
        $configuration->dropTable();
        $configuration->createTable();
    }


    public function testDelete_shouldDelete() {
        $entities = new Entities\Entities("entity", $this->pdo());

        $id = $entities->post(array('field1'=>'_1_', 'field2' => 2, "field3" => "harom"));

        $entities->delete($id);

        try {
            $entities->get($id);
        } catch(Entities\NotFoundException $e) { return; }
        $this->fail();
    }
}

?>