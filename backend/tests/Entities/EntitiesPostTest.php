<?php

class EntitiesPostTest extends Tests\Entities\BaseTestCase {

    public function setUp() {
        parent::setup();
        $configuration = new Entities\ConfigurationManagement("entity", $this->pdo());
        $configuration->dropTable();
        $configuration->createTable();
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testPost_throwsIllegalArgument_whenPropertiesIsEmpty() {
        $entities = new Entities\Entities("entity", $this->pdo());

        $entities->post(array());
    }


    public function testPost_shouldCreateEntity() {
        $entities = new Entities\Entities("entity", $this->pdo());

        $id = $entities->post(array(
                'field1' => 'fieldvalue1',
                'field2' => 'field2val',
                'field3' => null ));
        $result = $entities->get($id);

        $this->assertTrue(is_numeric($id));
        $this->assertEquals($result['id'], $id);
        $this->assertEquals($result['field1'], 'fieldvalue1');
        $this->assertEquals($result['field2'], 'field2val');
        $this->assertEquals($result['field3'], '');
    }

    public function testPost_shouldCreateEntity_withJsonDataType() {
        $this->addMeta('entity', 'jsonfield', 'type', 'json');
        $entities = new Entities\Entities("entity", $this->pdo());

        $id = $entities->post(array(
                'jsonfield' => array('assockey'=>'val') ));
        $result = $entities->get($id);

        $this->assertEquals($result['jsonfield'], array('assockey'=>'val'));
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testPost_shouldThrowException_WhenIdAttributePresent() {
        $entities = new Entities\Entities("entity", $this->pdo());

        $id = $entities->post(array('id' => 2));
    }


    /**
     * @expectedException InvalidArgumentException
     */
    public function testPost_shouldThrowException_whenInvalidKeyName() {
        $entities = new Entities\Entities("entity", $this->pdo());

        $id = $entities->post(array('' => 2));
    }

}

?>