<?php

class EntitiesMetaTest extends Tests\Entities\BaseTestCase {

    public function setUp() {
        parent::setup();
        $configuration = new Entities\ConfigurationManagement("entity", $this->pdo());
        $configuration->dropTable();
        $configuration->createTable();
    }


     public function testGetMeta_returnsParameters() {
        $entities = new Entities\Entities("entity", $this->pdo());
        $this->addMeta('entity', 'name1', 'prop1', 'value1');
        $this->addMeta('entity', 'name1', 'prop2', 'value2');
        $this->addMeta('entity', 'name2', 'prop1', 'value3');;

        $meta = $entities->getMetaParameters();

        $this->assertEquals($meta->getValues(), array(
            'name1' => array(
                'prop1' => 'value1',
                'prop2' => 'value2'),
            'name2' => array(
                'prop1' => 'value3')
            ));
    }


     public function testGetMeta_returnsEmpty() {
        $entities = new Entities\Entities("entity", $this->pdo());

        $meta = $entities->getMetaParameters();

        $this->assertEquals($meta->getValues(), array());
    }


     public function testPostMeta_addsMetaParameters() {
        $entities = new Entities\Entities("entity", $this->pdo());
        $entities->metaPost('a', array('k1'=>'v1', 'k2'=>'v2'));
        $entities->metaPost('b', array('k3'=>'v3', 'k4'=>'v4'));

        $meta = $entities->getMetaParameters();

        $this->assertEquals($meta->getValues(), array(
            'a' => array('k1'=>'v1', 'k2'=>'v2'),
            'b' => array('k3'=>'v3', 'k4'=>'v4'),
        ));
    }


    /**
     * @expectedException InvalidArgumentException
     */
    public function testPostMeta_throwsException_whenNameEmpty() {
        $entities = new Entities\Entities("entity", $this->pdo());

        $id = $entities->metaPost('', array('a' => 'b'));
    }


    public function testDeleteMeta_removesMetaParameter() {
        $entities = new Entities\Entities("entity", $this->pdo());
        $entities->metaPost('a', array('k1'=>'v1', 'k2'=>'v2'));
        $entities->metaPost('b', array('k3'=>'v3', 'k4'=>'v4'));

        $entities->metaDelete("a", "k2");
        $meta = $entities->getMetaParameters();

        $this->assertEquals($meta->getValues(), array(
            'a' => array('k1'=>'v1'),
            'b' => array('k3'=>'v3', 'k4'=>'v4'),
        ));
    }


    /**
     * @expectedException InvalidArgumentException
     */
    public function testMetaDelete_throwsException_whenNameEmpty() {
        $entities = new Entities\Entities("entity", $this->pdo());

        $id = $entities->metaDelete('', 'a');
    }


    /**
     * @expectedException InvalidArgumentException
     */
    public function testMetaDelete_throwsException_whenPropertyEmpty() {
        $entities = new Entities\Entities("entity", $this->pdo());

        $id = $entities->metaDelete('a', '');
    }


    public function testRenameMetaKey_renamesKey() {
        $entities = new Entities\Entities("entity", $this->pdo());
        $entities->metaPost('a', array('k1'=>'v1', 'k2'=>'v2'));
        $entities->metaPost('b', array('k3'=>'v3', 'k4'=>'v4'));

        $entities->metaKeyRename("a", "d");
        $meta = $entities->getMetaParameters();

        $this->assertEquals($meta->getValues(), array(
            'd' => array('k1'=>'v1', 'k2'=>'v2'),
            'b' => array('k3'=>'v3', 'k4'=>'v4'),
        ));
    }


    /**
     * @expectedException InvalidArgumentException
     */
    public function testRenameMetaKey_throwsException_whenFromEmpty() {
        $entities = new Entities\Entities("entity", $this->pdo());

        $id = $entities->metaKeyRename('', 'a');
    }


    /**
     * @expectedException InvalidArgumentException
     */
    public function testRenameMetaKey_throwsException_whenToEmpty() {
        $entities = new Entities\Entities("entity", $this->pdo());

        $id = $entities->metaKeyRename('a', '');
    }


    public function testMetaKeyDelete_deletesKey() {
        $entities = new Entities\Entities("entity", $this->pdo());
        $entities->metaPost('a', array('k1'=>'v1', 'k2'=>'v2'));
        $entities->metaPost('b', array('k3'=>'v3', 'k4'=>'v4'));

        $entities->metaKeyDelete('a');
        $meta = $entities->getMetaParameters();

        $this->assertEquals($meta->getValues(), array(
            'b' => array('k3'=>'v3', 'k4'=>'v4'),
        ));
    }


    /**
     * @expectedException InvalidArgumentException
     */
    public function testMetaKeyDelete_throwsException_whenFromEmpty() {
        $entities = new Entities\Entities("entity", $this->pdo());

        $id = $entities->metaKeyDelete('');
    }
}

?>