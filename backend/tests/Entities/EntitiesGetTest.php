<?php

class EntitiesGetTest extends Tests\Entities\BaseTestCase {

    public function setUp() {
        parent::setup();
        $configuration = new Entities\ConfigurationManagement("entity", $this->pdo());
        $configuration->dropTable();
        $configuration->createTable();
    }


    /**
     * @expectedException Entities\NotFoundException
     */
     public function testGet_throwsException_whenNotFound() {
        $entities = new Entities\Entities("entity", $this->pdo());

        $id = $entities->post(array('field1' => '_1_'));
        $entities->delete($id);
        $entities->get($id);
    }

    public function testGet_returnsOnlyId_whenAllColumnsFiltered() {
        $entities = new Entities\Entities("entity", $this->pdo());
        $id = $entities->post(array('field1' => '_1_', 'field2' => 'superbeat'));

        $entity = $entities->get($id, array('field3'));

        $this->assertEquals(array('id'=>$id), $entity);
    }

    public function testGet_returnsOnlyId_whenFilterEmptyArray() {
        $entities = new Entities\Entities("entity", $this->pdo());
        $id = $entities->post(array('field1' => '_1_', 'field2' => 'superbeat'));

        $entity = $entities->get($id, array());

        $this->assertEquals(array('id'=>$id), $entity);
    }

    public function testGet_returnsOnlyId_whenRequestedFieldNotExistsInDb() {
        $entities = new Entities\Entities("entity", $this->pdo());
        $id = $entities->post(array('field1' => '_1_', 'field2' => 'superbeat'));

        $entity = $entities->get($id, array('field3'));

        $this->assertEquals(array('id'=>$id), $entity);
    }

    /**
     * @expectedException Entities\NotFoundException
     */
     public function testGet_throwsException_whenFilterEmptyArrayAndNotFound() {
        $entities = new Entities\Entities("entity", $this->pdo());

        $id = $entities->post(array('field1' => '_1_'));
        $entities->delete($id);
        $entities->get($id, array());
    }


    public function testGet_returnsEntity() {
        $entities = new Entities\Entities("entity", $this->pdo());
        $id = $entities->post(array('field1' => '_1_', 'field2' => 'superbeat'));

        $entity = $entities->get($id);

        $this->assertEquals($entity['id'], $id);
        $this->assertEquals($entity['field1'], '_1_');
        $this->assertEquals($entity['field2'], 'superbeat');
    }

    public function testGet_returnsEntity_withQueriedFieldsOnly() {
        $entities = new Entities\Entities("entity", $this->pdo());
        $id = $entities->post(array('field1' => '_1_', 'field2' => 'superbeat', 'field3' => '3rd', 'field4' => '4th'));

        $entity = $entities->get($id, array('field1', 'field3'));

        $this->assertArrayNotHasKey('field2', $entity);
        $this->assertArrayNotHasKey('field4', $entity);
        $this->assertEquals($entity['field1'], '_1_');
        $this->assertEquals($entity['field3'], '3rd');
    }

    public function testGet_doesNotThrowNotFound_whenOnlyQueriedFieldNotFound() {
        $entities = new Entities\Entities("entity", $this->pdo());
        $id = $entities->post(array('field1' => '_1_'));

        $entity = $entities->get($id, array('field3'));

        $this->assertArrayNotHasKey('field1', $entity);
        $this->assertArrayNotHasKey('field3', $entity);
    }

    public function testGetAll_returnsArrayOfEntity() {
        $entities = new Entities\Entities("entity", $this->pdo());
        $id1 = $entities->post(array('field1' => '_1_', 'field2' => 'superbeat'));
        $id2 = $entities->post(array('a' => 'b', 'c' => 'd'));

        $entities = $entities->getAll();

        $this->assertEquals(count($entities), 2);
        $this->assertEquals($entities[0]['id'], $id1);
        $this->assertEquals($entities[0]['field1'], '_1_');
        $this->assertEquals($entities[0]['field2'], 'superbeat');
        $this->assertEquals($entities[1]['id'], $id2);
        $this->assertEquals($entities[1]['a'], 'b');
        $this->assertEquals($entities[1]['c'], 'd');
    }

    public function testGetAll_returnsJson_whenJsonDataTypeIsDefined() {
        $entities = new Entities\Entities("entity", $this->pdo());
        $this->addMeta('entity', 'a', 'type', 'json');
        $id = $entities->post(array('a' => array('abc', 'bcd'), 'c' => 'd'));

        $entities = $entities->getAll();

        $this->assertEquals($entities[0], array( 'id'=>$id, 'a' => array('abc', 'bcd'), 'c'=>'d'));
    }

    public function testGet_returnsJson_whenJsonDataTypeIsDefined() {
        $entities = new Entities\Entities("entity", $this->pdo());
        $this->addMeta('entity', 'a', 'type', 'json');
        $id = $entities->post(array('a' => array('ab', 'bc'), 'c' => 'd'));

        $entities = $entities->get($id, array('a', 'c'));

        $this->assertEquals($entities, array( 'id'=>$id, 'a' => array('ab', 'bc'), 'c'=>'d'));
    }

    public function testGetBy_returnsOk_whenExists() {
        $entities = new Entities\Entities("entity", $this->pdo());
        $id = $entities->post(array('field1' => 'f1', 'field2' => 'f2'));
        $entities->post(array('field1' => 'f11', 'field2' => 'f22'));

        $entity = $entities->getBy('field1', 'f1');

        $this->assertEquals(array('id' => $id, 'field1' => 'f1', 'field2' => 'f2'), $entity);
    }


    public function testGetBy_returnsListedFields_whenOk() {
        $entities = new Entities\Entities("entity", $this->pdo());
        $id = $entities->post(array('field1' => 'f1', 'field2' => 'f2', 'nofield' => 'abc'));

        $entity = $entities->getBy('field1', 'f1', array('field1'));

        $this->assertEquals(array('id' => $id, 'field1' => 'f1'), $entity);
    }

    /**
     * @expectedException Entities\NotFoundException
     */
    public function testGetBy_throwsException_whenMultipleEntitiesMatched() {
        $entities = new Entities\Entities("entity", $this->pdo());
        $entities->post(array('field1' => 'f1', 'field2' => 'f2'));
        $entities->post(array('field1' => 'f1', 'field2' => 'f22'));

        $entity = $entities->getBy('field1', 'f1');
    }

    /**
     * @expectedException Entities\NotFoundException
     */
    public function testGetBy_throwsNotFoundException_whenNoMatchingEntityExists() {
        $entities = new Entities\Entities("entity", $this->pdo());
        $id = $entities->post(array('field1' => 'f1', 'field2' => 'f2', 'nofield' => 'abc'));

        $entity = $entities->getBy('field1', 'f2');
    }
}

?>