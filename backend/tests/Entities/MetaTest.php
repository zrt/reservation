<?php

namespace Tests\Entities;

class MetaTest extends \PHPUnit_Framework_TestCase
{
    public function testIs() {
        $meta = new \Entities\Meta(array('name1' => array('parameter'=>'paramvalue')));;

        $this->assertTrue($meta->is('name1', 'parameter', 'paramvalue'));
        $this->assertTrue($meta->is('name1', 'parameter', 'ParamValue'));

        $this->assertFalse($meta->is('name', 'parameter', 'ParamValue'));
        $this->assertFalse($meta->is('name', 'parameter1', 'ParamValue'));
        $this->assertFalse($meta->is('name1', 'parameter1', 'ParamValue'));
        $this->assertFalse($meta->is('name1', 'parameter', 'paramval'));
    }
}

?>