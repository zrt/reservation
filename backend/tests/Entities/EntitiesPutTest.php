<?php

class EntitiesPutTest extends Tests\Entities\BaseTestCase {

    public function setUp() {
        parent::setup();
        $configuration = new Entities\ConfigurationManagement("entity", $this->pdo());
        $configuration->dropTable();
        $configuration->createTable();
    }


    /**
     * @expectedException InvalidArgumentException
     */
    public function testPut_throwsIllegalArgument_whenPropertiesIsEmpty() {
        $entities = new Entities\Entities("entity", $this->pdo());

        $entities->put(1, array());
    }


    /**
     * @expectedException InvalidArgumentException
     */
    public function testPut_shouldThrowException_WhenIdAttributePresent() {
        $entities = new Entities\Entities("entity", $this->pdo());

        $id = $entities->put(1, array('id' => 2));
    }

    public function testPut_acceptsId_whenEqualsWithFirstParam() {
        $entities = new Entities\Entities("entity", $this->pdo());

        $id = $entities->put(1, array('id' => 1));
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testPut_shouldThrowException_whenInvalidKeyName() {
        $entities = new Entities\Entities("entity", $this->pdo());

        $id = $entities->put(1, array('' => 2));
    }


    public function testPut_shouldChangeAndInsert() {
        $entities = new Entities\Entities("entity", $this->pdo());

        $id = $entities->post(array('field1'=>'_1_', 'field2' => 2, "field3" => "harom"));

        $entities->put($id, array(
                'field1' => '2',
                'field2' => 'update',
                'field4' => 'beat'));

        $result = $entities->get($id);
        $this->assertEquals($result['id'], $id);
        $this->assertEquals($result['field1'], '2');
        $this->assertEquals($result['field2'], 'update');
        $this->assertEquals($result['field3'], 'harom');
        $this->assertEquals($result['field4'], 'beat');
    }

    public function testPut_shouldChangeAndInsert_whenDataTypeIsJson() {
        $entities = new Entities\Entities("entity", $this->pdo());
        $this->addMeta('entity', 'jsondata', 'type', 'json');

        $id = $entities->post(array('field1'=>'1'));

        $entities->put($id, array(
                'jsondata' => array('key'=>'value')
            ));

        $result = $entities->get($id);
        $this->assertEquals($result['jsondata'], array('key'=>'value'));
    }
}

?>