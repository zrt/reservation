<?php

namespace Tests\Entities;

abstract class BaseTestCase extends \PHPUnit_Framework_TestCase
{
    // only instantiate once for test clean-up/fixture load
    static private $pdo = null;

    // only instantiate once per test
    private $conn = null;

    public function pdo()
    {
        if ($this->conn === null) {
            if (self::$pdo == null) {
                self::$pdo = new \PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
                self::$pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            }
            $this->conn = self::$pdo;
        }

        return $this->conn;
    }

    public function addMeta($entity, $name, $property, $value) {
        $pdo = $this->pdo();
        $stmt = $pdo->prepare("INSERT INTO ${entity}_meta VALUES(:name, :property, :value);");
        $stmt->bindParam('name', $name);
        $stmt->bindParam('property', $property);
        $stmt->bindParam('value', $value);
        $stmt->execute();
    }
}
?>