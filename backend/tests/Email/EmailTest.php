<?php

namespace Email;

class EmailTest extends \PHPUnit_Framework_TestCase {
    protected $app;

    public function setUp() {
        $this->app = new \Silex\Application();

        $this->app['mailer'] = $this->getMockBuilder('\Swift_Mailer')->disableOriginalConstructor()->getMock();
        $this->app['template'] = $this->getMockBuilder('\Email\Template')->disableOriginalConstructor()->getMock();
        $this->app['mailer-message'] = $this->getMockBuilder('\Swift_Message')->disableOriginalConstructor()->getMock();
        $this->app['mailer-message']->method('setSubject')->willReturn($this->app['mailer-message']);
        $this->app['mailer-message']->method('setFrom')->willReturn($this->app['mailer-message']);
        $this->app['mailer-message']->method('setTo')->willReturn($this->app['mailer-message']);
        $this->app['mailer-message']->method('setBody')->willReturn($this->app['mailer-message']);
    }


    public function testSend_callsSwift_withTemplateAndSubject() {
        $this->app['config'] = array('email' => array(
            'enabled' => true,
            'from' => 'from-addr'
        ));
        $email = new Email($this->app);
        $this->app['mailer']
            ->expects($this->once())
            ->method('send')
            ->with($this->app['mailer-message']);
        $this->app['mailer-message']->expects($this->once())->method('setSubject')->with('compiled-subject');
        $this->app['mailer-message']->expects($this->once())->method('setFrom')->with(array('from-addr'));
        $this->app['mailer-message']->expects($this->once())->method('setTo')->with(array('email'));
        $this->app['mailer-message']->expects($this->once())->method('setBody')->with('compiled-body');
        $model = array();

        $this->app['template']
            ->expects($this->exactly(2))
            ->method('compile')
            ->will($this->returnValueMap(
                    [['template.subject', $model, 'compiled-subject'],
                     ['template.body', $model, 'compiled-body']]));

        $email->send('template', $model, 'email');
    }



    public function testSend_doesNothing_whenNotEnabledInConfig() {
        $model = array();
        $this->app['config'] = array('email' => array(
            'enabled' => false,
        ));
        $this->app['mailer']->expects($this->never())->method('send');

        $email = new Email($this->app);

        $email->send('template', $model, 'email');
    }
}

?>