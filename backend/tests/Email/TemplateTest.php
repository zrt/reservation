<?php

namespace Email;

class TemplateTest extends \PHPUnit_Framework_TestCase {
    protected $app;
    protected $template;

    public function setUp() {
        $this->app = new \Silex\Application();
        $this->template = new Template($this->app);
    }


    public function testCompile_canLoadAndRender_DefaultUnloadTemplate() {
        $model = array( 'user' => array('username' => 'uname'),
                        'target' => array('name' => 'targetname'));

        $body = $this->template->compile('unlock.body', $model);
        $subject = $this->template->compile('unlock.subject', $model);

        $this->assertContains('Hi uname', $body);
        $this->assertContains('Target targetname', $subject);
    }
}

?>