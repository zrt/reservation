<?php

namespace Email;

class Template {
    private $app;
    private $templateEngine;


    public function __construct(\Silex\Application $app) {
        $this->app = $app;
        $this->templateEngine = new \Mustache_Engine(array(
            'loader' => new \Mustache_Loader_FilesystemLoader(dirname(__FILE__) . '/../../templates/'),
        ));
    }


    public function compile($template, &$model) {
        return $this->templateEngine->render($template, $model);
    }
}

?>