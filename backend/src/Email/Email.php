<?php

namespace Email;

class Email {
    private $app;

    public function __construct(\Silex\Application $app) {
        $this->app = $app;

        if ( !$this->isEnabled() )
            return;

        $app['swiftmailer.use_spool'] = false;
        $app['swiftmailer.options'] = $app['config']['email'];
        if ( empty($app['mailer-message']) )
            $app['mailer-message'] = function() {
                return \Swift_Message::newInstance();
            };
    }



    public function send($template, $model, $recipient) {

        if ( !$this->isEnabled() )
            return;

        $subject = $this->app['template']->compile($template . '.subject', $model);
        $body = $this->app['template']->compile($template . '.body', $model);

        $message = $this->app['mailer-message']
            ->setSubject($subject)
            ->setBody($body)
            ->setFrom(array($this->app['config']['email']['from']))
            ->setTo(array($recipient));

        // activate only when send
        if ( !empty($this->app['config']['email']['debug']) ) {
            $logger = new \Swift_Plugins_Loggers_EchoLogger();
            $this->app['mailer']->registerPlugin(new \Swift_Plugins_LoggerPlugin($logger));
        }

        // setlocaldomain added for microsoft smtp server.
        // TODO find out why this is needed
        $this->app['swiftmailer.transport']->setLocalDomain('[127.0.0.1]');
        $this->app['mailer']->send($message);
    }



    protected function isEnabled() {
        return !empty($this->app['config']['email']['enabled']);
    }
}

?>