<?php

namespace User;



class User {

    protected $username;

    public function load($username) {
        if ( empty($username) )
            throw new \InvalidArgumentException("Username empty");
        $this->username = $username;
    }

    public function getUsername() {
        return $this->username;
    }

    public function hasZone($zone) {
        return strcasecmp($this->username, "admin") == 0
            || strcasecmp($this->username, $zone) == 0
            || ( strcasecmp("user", $zone) == 0 && !empty($this->username) );
    }
}

?>