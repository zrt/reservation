<?php

namespace Controllers;

/**
 * Thrown when user is not authorized for an action
 */
class UnauthorizedException extends \Exception
{
    public function __construct($message, $code = 0, Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}

?>