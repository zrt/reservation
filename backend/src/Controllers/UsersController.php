<?php

    namespace Controllers;

    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpFoundation\Request;
    use Silex\Application;


    class UsersController {
        private $app;


        public function __construct(Application $app) {
            $this->app = $app;
        }


        public function get($username) {
            $reply = $this->app['users']->getBy('username', $username);
            unset($reply['id']);
            return $reply;
        }


        public function put($username, Request $request) {
            $data = $request->request->all();
            $data['username'] = $username;    // override username

            try {
                $id = $this->app['users']->getBy('username', $username)['id'];
                $this->app['users']->put($id, $data);
            } catch(\Entities\NotFoundException $ex) {
                $id = $this->app['users']->post($data);
            }

            return $this->app['users']->get($id);
        }

    };


?>