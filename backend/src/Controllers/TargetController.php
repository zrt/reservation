<?php

    namespace Controllers;

    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpFoundation\Request;
    use Silex\Application;


    class TargetController {
        private $app;


        public function __construct(Application $app) {
            $this->app = $app;
        }


        public function getAll() {
            return array(
                'entries' => $this->app['targets']->getAll(),
                'meta' => $this->app['targets']->getMetaParameters()->getValues()
            );
        }


        public function getById($id) {
            return $this->app['targets']->get($id);
        }


        public function delete($id) {
            $this->app['targets']->delete($id);
            return new Response();
        }


        public function put($id, Request $request) {
            $data = $request->request->all();
            $this->app['targets']->put($id, $data);
            return $this->app['targets']->get($id);
        }


        public function post(Request $request) {
            $data = $request->request->all();
            $id = $this->app['targets']->post($data);
            return $this->app['targets']->get($id);
        }


        /**
         * Adds a user to the queue
         */
        public function lock($id) {
            $username = $this->app['user']->getUsername();

            if ( empty($username) )
                throw new UnauthorizedException("No user provided");

            $var = $this->app['targets']->get($id, array('locked'));
            if (empty($var['locked'])) {
                $var['locked'] = array();
            }

            if (!is_array($var['locked'])) {
                // Invalid data in database
                $var['locked'] = array();
            }

            // already locked
            if ( array_search($username, $var['locked']) === false ) {
                array_push($var['locked'], $username);
                $this->app['targets']->put($id, $var);
            }

            return array('status' => 'ok');
        }


        /**
         * removes a user from the lock queue
         */
        public function unlock($id) {
            $username = $this->app['user']->getUsername();
            if ( empty($username) )
                throw new UnauthorizedException("No user provided");

            $ok = array('status' => 'ok');

            $var = $this->app['targets']->get($id, array('locked'));
            if ( empty($var['locked']) )
                return $ok;

            $changed = false;
            if (!is_array($var['locked'])) {
                // Invalid data in database
                $changed = true;
                $var['locked'] = array();
            }

            // collect the list of users not matching username, case insensitively
            $result = array();
            foreach ( $var['locked'] as $locked ) {
                if ( strcasecmp($locked, $username) != 0 ) {
                    array_push($result, $locked);
                } else {
                    $changed = true;
                }
            }

            // don't save when nothing changed
            if ( $changed ) {
                $this->app['targets']->put($id, array('locked' => $result));
                if ( count($result) > 0 ) {
                    try {
                        // result contains list of users who locks the target
                        // 0 item is the new owner
                        $user = $this->app['users']->getBy('username', $result[0]);
                        if ( !empty($user['email']) )
                            $this->app['email']->send("unlock", array('user' => $user, 'target' => $this->app['targets']->get($id)), $user['email']);
                    } catch(\Exception $ex) { /* intentional */}
                }
            }

            return $ok;
        }


        public function metaKeyDelete($key) {
            $this->app['targets']->metaKeyDelete($key);
            return array('status' => 'ok');
        }


        public function metaKeyRename($key, Request $request) {
            if ( empty($request->request->all()['to']) )
                throw new \InvalidArgumentException("Empty to field");
            $this->app['targets']->metaKeyRename($key, $request->request->all()['to']);
            return array('status' => 'ok');
        }


        public function metaDelete($key, $meta) {
            $this->app['targets']->metaDelete($key, $meta);
            return array('status' => 'ok');
        }


        public function metaPost($key, Request $request) {
            $data = $request->request->all();
            $this->app['targets']->metaPost($key, $data);
            return array('status' => 'ok');
        }

    };


?>