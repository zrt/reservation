<?php

    require_once __DIR__.'/../vendor/autoload.php';

    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpFoundation\Request;

    $app = new Silex\Application();
    $app->register(new \Silex\Provider\SwiftmailerServiceProvider());
    $app->register(new Silex\Provider\ServiceControllerServiceProvider());

    $config = json_decode(@file_get_contents(__DIR__ . '/config.json'), true);
    if ( !empty($config) ) {
        $app['config'] = $config;
    }

    $app['pdo'] = $app->share(function() use ($app) {
        $conf = $app['config']['database'];
        $pdo = new PDO($conf['dsn'], $conf['user'], $conf['password']);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $pdo;
    });

    $app['targets'] = function($c) {
        return new Entities\AccessControlledEntities('target', $c['pdo'], $c['user']);
    };

    $app['users'] = function($c) {
        return new Entities\AccessControlledEntities('user', $c['pdo'], $c['user']);
    };

    $app['user'] = $app->share(function() {
        return new \User\User();
    });

    $app['template'] = $app->share(function($app) {
        return new \Email\Template($app);
    });

    $app['email'] = $app->share(function($app) {
        return new \Email\Email($app);
    });

    $app->before(function (Request $request) use ($app) {
        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            $data = json_decode($request->getContent(), true);
            $request->request->replace(is_array($data) ? $data : array());
        }

        $authHeader = $request->headers->get('Authorization');
        if ( !empty($authHeader) ) {
            $x = explode(' ', $authHeader);
            if ( count($x) != 2 || $x[0] != "basic" )
                throw new \InvalidArgumentException("Authorization must be basic");
            $decoded = base64_decode($x[1]);
            if ( $decoded === false )
                throw new \InvalidArgumentException("Invalid authorization header");
            $app['user']->load(explode(':', $decoded)[0]);
        }
    });

    $app->view(function (array $controllerResult) use ($app) {
        return $app->json($controllerResult);
    });

    /*
     * Set up targets controller
     */

    $app['targetController'] = $app->share(function() use ($app) {
        return new Controllers\TargetController($app);
    });

    $targets = $app['controllers_factory'];
    $targets->get('/', 'targetController:getAll');
    $targets->post('/', 'targetController:post');
    $targets->get('/{id}', 'targetController:getById')
        ->assert('id', '\d+');
    $targets->delete('/{id}', 'targetController:delete')
        ->assert('id', '\d+');
    $targets->put('/{id}', 'targetController:put')
        ->assert('id', '\d+');
    $targets->post('/{id}/lock', 'targetController:lock')
        ->assert('id', '\d+');
    $targets->post('/{id}/unlock', 'targetController:unlock')
        ->assert('id', '\d+');

    $targets->delete('/meta/{key}', 'targetController:metaKeyDelete');
    $targets->patch('/meta/{key}', 'targetController:metaKeyRename');
    $targets->post('/meta/{key}', 'targetController:metaPost');
    $targets->delete('/meta/{key}/{meta}', 'targetController:metaDelete');

    $app->mount('/targets', $targets);


    /*
     * Set up users controller
     */
    $app['UsersController'] = $app->share(function() use ($app) {
        return new Controllers\UsersController($app);
    });

    $users = $app['controllers_factory'];
    $users->get('/{username}', 'UsersController:get');
    $users->put('/{username}', 'UsersController:put');
    $app->mount('/users', $users);



    /*
     * Set up error handler
     */
    $app->error(function($exception) {

        $response = new Response();

        if (method_exists($exception, 'getStatusCode')) {
            $response->setStatusCode($exception->getStatusCode());
        } else {
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $response->setContent(json_encode(array(
            'exception' => array(
                'message' => $exception->getMessage()
            )
        )));

        return $response;
    }, 0);


    // unittest requires the app instance to run
    if ( isset($app_test) )
        return $app;
    $app->run();
  ?>