<?php

    namespace Installer;

    require_once __DIR__.'/../vendor/autoload.php';

    echo '<pre>';

    $config = json_decode(@file_get_contents(__DIR__ . '/config.json'), true);
    if ( empty($config) )
        die("Unable to load config.json");


    $pdo = new \PDO($config['database']['dsn'],$config['database']['user'], $config['database']['password']);
    $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

    try
    {
        $users = new \Entities\ConfigurationManagement('user', $pdo);
        $targets = new \Entities\ConfigurationManagement('target', $pdo);

        $users->createTable();
        $targets->createTable();
    } catch(\Exception $e) {
        echo "Exception occurred during installation\n";
        print_r($e);
    }
?>