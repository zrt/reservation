<?php

namespace Entities;

define('ZONES_VIEW', 'zones.view');


class AccessControlledEntities extends Entities {

    protected $currentUser;


    public function __construct($entityType, \PDO $pdo, \User\User $currentUser) {
        Entities::__construct($entityType, $pdo);
        $this->currentUser = $currentUser;
    }


    public function getAll() {
        $allowed = array();
        $denied = array();
        $this->metaForZone(ZONES_VIEW, $allowed, $denied);

        $result = Entities::getAll();

        foreach( $result as &$rows )
            foreach( $rows as $key => &$value )
                if ( array_key_exists($key, $denied) )
                    unset($rows[$key]);

        return $result;
    }


    public function get($id, array $fields = null) {
        $allowed = array();
        $denied = array();
        $this->metaForZone(ZONES_VIEW, $allowed, $denied);

        if ( !empty($fields) ) {
            // when fields defined query params with the diff of allowed & the given fields
            $allowed = array_intersect(array_keys($allowed), $fields);
            return Entities::get($id, $allowed);
        }

        // otherwise query all and substract fields which are not allowed
        // as there can be fields which are not listed in meta but query should show them too
        $result = Entities::get($id);
        foreach ($denied as $key => &$meta) {
            if ( array_key_exists($key, $result) )
                unset($result[$key]);
        }

        return $result;
    }


    public function getBy($key, $value, array $fields = null) {
        $this->checkRestrictedFields(array($key => ''));
        return Entities::getBy($key, $value, $fields);
    }


    public function delete($id) {
        $allowed = array();
        $denied = array();
        $this->metaForZone(ZONES_VIEW, $allowed, $denied);
        if ( !empty($denied) )
            throw new ForbiddenException("Delete restricted because of fields [" . implode(',', array_keys($denied)) . "]");

        return Entities::delete($id);
    }


    public function put($id, $properties) {
        $this->checkRestrictedFields($properties);
        return Entities::put($id, $properties);
    }


    /**
     * Throws ForbiddenException if any of the parameter is restricted
     */
    public function post($properties) {
        $this->checkRestrictedFields($properties);
        return Entities::post($properties);
    }


    /**
     * Returns list of metaparameters which is viewable for the current user
     */
    public function getMetaParameters() {
        $allowed = array();
        $denied = array();
        $this->metaForZone(ZONES_VIEW, $allowed, $denied);

        return new Meta($allowed);
    }


    protected function metaForZone($zone, array &$allowed, array &$denied) {
        $meta = Entities::getMetaParameters();
        foreach( $meta->getValues() as $key => &$col ) {
            if ( !empty($col[$zone]) && !$this->currentUser->hasZone($col[$zone]) ) {
                $denied[$key] = $col;
            } else {
                $allowed[$key] = $col;
            }
        }
    }


    /**
     * Throws exception when restricted fields were found
     * @param $properties associative array where array-keys are the keys << TODO refactor to have simple array
     */
    protected function checkRestrictedFields(array $properties) {
        $allowed = array();
        $denied = array();
        $this->metaForZone(ZONES_VIEW, $allowed, $denied);

        $not_allowed_fields = array_intersect(array_keys($denied), array_keys($properties));
        if ( !empty($not_allowed_fields) )
            throw new ForbiddenException("Fields [" . implode(',', $not_allowed_fields) . "] are restricted");
    }

}

 ?>