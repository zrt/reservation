<?php

    namespace Entities;

    class ConfigurationManagement {
        private $pdo;
        private $entityType;


        public function __construct($entityType, \PDO $pdo) {
            $this->pdo = $pdo;
            $this->entityType = $entityType;
        }


        public function createTable() {
            $sql = "CREATE TABLE `". $this->entityType ."_parameters` (
                      `id` int(11) NOT NULL AUTO_INCREMENT,
                      `name` varchar(32) NOT NULL,
                      `content` text,
                      PRIMARY KEY (`id`, `name`),
                      KEY `id` (`id`)
                    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

                    CREATE TABLE IF NOT EXISTS ". $this->entityType ."_meta (
                      `name` varchar(64) CHARACTER SET utf8 NOT NULL,
                      `property` varchar(64) CHARACTER SET utf8 NOT NULL,
                      `value` varchar(64) CHARACTER SET utf8 NOT NULL,
                      PRIMARY KEY (`name`,`property`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
                    ";

            $this->execute($sql);
        }


        public function dropTable() {
            $sql = "DROP TABLE IF EXISTS ". $this->entityType ."_parameters;
                    DROP TABLE IF EXISTS ". $this->entityType ."_meta;";
            $this->execute($sql);
        }


        protected function execute($sql) {
            $sqls = explode(';', $sql);
            try {
                $this->pdo->beginTransaction();

                foreach($sqls as $sql_command) {
                    $sql_command = trim($sql_command);
                    if ( empty($sql_command) )
                        continue;
                    $statement = $this->pdo->prepare($sql_command);
                    $statement->execute();
                }

                $this->pdo->commit();
            } catch (\Exception $e) {
                $this->pdo->rollback();
                throw $e;
            }
        }
    }
?>