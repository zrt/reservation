<?php

namespace Entities;

/**
 * Thrown when entity is not found
 */
class ForbiddenException extends \Exception
{
    public function __construct($message, $code = 0, Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }

    public function getStatusCode() { return 403; }
}

?>