<?php

    namespace Entities;

    class Meta {
        private $values;

        public function __construct(array $values) {
            $this->values = $values;
        }

        public function getValues() {
            return $this->values;
        }

        /**
         * Case insensitive check if a meta parameter equals to a value
         */
        public function is($name, $parameter, $value) {
            return array_key_exists($name, $this->values)
                && array_key_exists($parameter, $this->values[$name])
                && strcasecmp($this->values[$name][$parameter], $value) == 0;
        }
    };

?>