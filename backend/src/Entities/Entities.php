<?php

    namespace Entities;

    class Entities {
        protected static $META_TYPE = 'type';

        protected $entityType;
        protected $pdo;
        protected $meta = null;

        /**
         * Constructs an entity store
         * @param entityType string name of an entity class
         * @param pdo the database connection object
         */
        public function __construct($entityType, \PDO $pdo) {
            $this->entityType = $entityType;
            $this->pdo = $pdo;
        }

        /**
         * Retrieves all the entities with all of their properties
         */
        public function getAll() {
            $stmt = $this->execute('SELECT id, name, content FROM '.$this->entityType.'_parameters', array());

            $result = array();
            while ( $row = $stmt->fetch(\PDO::FETCH_ASSOC) ) {
                if ( !array_key_exists($row['id'], $result) )
                    $result[$row['id']] = array('id' => $row['id']);
                $this->loadValue($result[$row['id']], $row);
            }

            return array_values($result);
        }

        /**
         * Adds a fetchedAssoc row of parameter table to the result array where result array is one record
         */
        protected function loadValue(array &$result, $fetchedAssoc) {
            $meta = $this->getMetaParameters();
            if ( $meta->is($fetchedAssoc['name'], static::$META_TYPE, 'json') )
                $value = json_decode($fetchedAssoc['content'], true);
            else
                $value = $fetchedAssoc['content'];

            $result[$fetchedAssoc['name']] = $value;
        }

        protected function bindContent($statement, $name, $content) {
            $meta = $this->getMetaParameters();

            if ( $meta->is($name, static::$META_TYPE, 'json') )
                $value = json_encode($content);
            else
                $value = &$content;

            $statement->bindParam('content', $value);
        }

        protected function isExists($id) {
            $check_stmt = $this->execute('SELECT id FROM '.$this->entityType.'_parameters WHERE id = :id LIMIT 1', array('id'=>$id));
            return $check_stmt->rowCount() != 0;
        }

        /**
         * Retrieves all properties of an existing element
         */
        public function get($id, array $fields = null) {
            $res = array( 'id' => $id );
            $field_where = '';
            if ( !empty($fields) ) {
                $field_where = ' AND name IN ("' . implode('","', $fields) .'")';
            } else if ( $fields !== null ) {
                if ( !$this->isExists($id) )
                    throw new NotFoundException($id);
                return $res;
            }

            $stmt = $this->execute('SELECT id, name, content FROM '.$this->entityType.'_parameters WHERE id = :id' . $field_where, array( 'id' => $id ));

            if ( $stmt->rowCount() == 0 ) {
                if ( !$this->isExists($id) )
                    throw new NotFoundException($id);
            }

            while ( $row = $stmt->fetch(\PDO::FETCH_ASSOC) ) {
                $this->loadValue($res, $row);
            }

            return $res;
        }


        /**
         * Returns properties of an element where element is identified by
         * a field and its value.
         * @throws exception when multiple records exists for the given key
         */
        public function getBy($key, $value, array $fields = null) {
            $id = $this->findIdBy($key, $value);
            return $this->get($id, $fields);
        }


        protected function findIdBy($key, $value) {
            $stmt = $this->execute('SELECT id FROM '.$this->entityType.'_parameters WHERE name = :key AND content = :value LIMIT 2', array( 'key' => $key, 'value' => $value ));

            if ( $stmt->rowCount() == 0 )
                throw new NotFoundException($key . '=' . $value);

            if ( $stmt->rowCount() > 1 ) {
                throw new NotFoundException("results are not unique");
            }

            return $stmt->fetch(\PDO::FETCH_NUM)[0];
        }

        /**
         * Deletes an existing element
         */
        public function delete($id) {
            $this->execute('DELETE FROM '.$this->entityType.'_parameters WHERE id = :id;', array( 'id' => $id ));
        }

        /**
         * Adds/updates properties of an existing element
         */
        public function put($id, $properties) {
            if ( count($properties) == 0 )
                throw new \InvalidArgumentException("Properties cannot be empty");

            if ( array_key_exists('id', $properties) && $properties['id'] != $id )
                throw new \InvalidArgumentException("Id attribute is reserved");

            $stmt = $this->execute('SELECT name FROM '.$this->entityType.'_parameters WHERE id = :id', array( 'id' => $id ));

            $res = $stmt->fetchAll(\PDO::FETCH_NUM);
            $existing_names = array();
            foreach ($res as &$val)
                $existing_names[] = $val[0];

            $insert = $this->pdo->prepare('INSERT INTO '.$this->entityType.'_parameters(id, name, content) VALUES(:id, :name, :content)');
            $insert->bindParam("id", $id);

            $update = $this->pdo->prepare('UPDATE '.$this->entityType.'_parameters SET content = :content WHERE id = :id AND name = :name');
            $update->bindParam("id", $id);
            if ( count($properties) == 0 )
                return;

            try {
                $this->beginTransaction();

                foreach ($properties as $name => &$content) {
                    if ( empty($name) )
                        throw new \InvalidArgumentException("Invalid attribute name");

                    $theStatement = array_search($name, $existing_names) === false ? $insert : $update;
                    $theStatement->bindParam("name", $name);
                    $this->bindContent($theStatement, $name, $content);
                    $theStatement->execute();
                    // todo: is exception thrown when not update/inserted?
                }

                $this->commit();
            } catch(\Exception $e) {
                $this->rollback();
                throw $e;
            }
        }

        /**
         * Adds a new element to the database
         * @return id of the newly inserted element
         */
        public function post($properties) {
            if ( count($properties) == 0 )
                throw new \InvalidArgumentException("Properties cannot be empty");

            if ( array_key_exists('id', $properties) )
                throw new \InvalidArgumentException("Id attribute is reserved");

            try {
                $this->beginTransaction();

                $stmt = $this->pdo->prepare('INSERT INTO '.$this->entityType.'_parameters(id, name, content) VALUES(:id, :name, :content)');
                $id = null;
                $stmt->bindParam('id', $id);
                foreach ($properties as $name => &$content) {
                    if ( empty($name) )
                        throw new \InvalidArgumentException("Invalid attribute name");

                    if ( $id === null ) {
                        $insert_stmt = $this->pdo->prepare('INSERT INTO '.$this->entityType.'_parameters(name, content) VALUES(:name, :content)');
                        $insert_stmt->bindParam('name', $name);
                        $this->bindContent($insert_stmt, $name, $content);
                        $insert_stmt->execute();
                        $id = $this->pdo->lastInsertId();
                        continue;
                    }

                    $stmt->bindParam('name', $name);
                    $this->bindContent($stmt, $name, $content);
                    $stmt->execute();
                }

                $this->commit();
                return $id;
            } catch(\Exception $e) {
                $this->rollback();
                throw $e;
            }
        }

        /**
         * Returns an executed PDOStatement.
         * @param query string sql query
         * @param parameters array of key-value pairs which are bound to the statement in the form of ':key'
         */
        protected function execute($query, $parameters) {
            $stmt = $this->pdo->prepare($query);
            foreach ($parameters as $key => &$value)
                $stmt->bindParam($key, $value);

            $stmt->execute();
            return $stmt;
        }

        protected function beginTransaction() {
            //echo "begin\n";
            //var_dump(debug_backtrace()[1]);
            $this->pdo->beginTransaction();
        }

        protected function commit() {
            //echo "commit\n";
            $this->pdo->commit();
        }

        protected function rollback() {
            //echo "rollback\n";
            $this->pdo->rollback();
        }

        /**
         * Returns an array of parameters. The top level keys are the entity names and the value is an array for parameters -> value
         */
        public function getMetaParameters() {
            if ( $this->meta != null )
                return $this->meta;

            $stmt = $this->pdo->prepare('SELECT * FROM '.$this->entityType.'_meta');
            $stmt->execute();

            $params = array();
            while ( $row = $stmt->fetch(\PDO::FETCH_ASSOC) ) {
                if ( !isset($params[$row['name']]) ) {
                    $params[$row['name']] = array();
                }

                $params[$row['name']][$row['property']] = $row['value'];
            }
            $this->meta = new Meta($params);

            return $this->meta;
        }


        public function metaKeyDelete($name) {
            if ( empty($name) )
                throw new \InvalidArgumentException("Invalid name");

            $stmt = $this->pdo->prepare('DELETE FROM '.$this->entityType.'_meta WHERE name = :name');
            $stmt->bindParam('name', $name);
            $stmt->execute();
        }


        public function metaKeyRename($fromName, $toName) {
            if ( empty($fromName) || empty($toName) )
                throw new \InvalidArgumentException("Invalid parameters");

            $stmt = $this->pdo->prepare('UPDATE '.$this->entityType.'_meta SET name = :toname WHERE name = :fromname');
            $stmt->bindParam('fromname', $fromName);
            $stmt->bindParam('toname', $toName);
            $stmt->execute();
        }


        public function metaDelete($name, $property) {
            if ( empty($name) || empty($property) )
                throw new \InvalidArgumentException("Invalid parameters");

            $stmt = $this->pdo->prepare('DELETE FROM '.$this->entityType.'_meta WHERE name = :name AND property = :property');
            $stmt->bindParam('name', $name);
            $stmt->bindParam('property', $property);
            $stmt->execute();
        }


        public function metaPost($name, Array $values) {
            if ( empty($name) || empty($values) )
                throw new \InvalidArgumentException("Invalid parameters");

            try {
                $this->beginTransaction();

                $stmt = $this->pdo->prepare('REPLACE INTO '.$this->entityType.'_meta(name, property, value) VALUES(:name, :property, :value)');
                $stmt->bindParam('name', $name);

                foreach ($values as $property => &$value) {
                    if ( empty($property) )
                        throw new \InvalidArgumentException("Invalid meta property");

                    $stmt->bindParam('property', $property);
                    $stmt->bindParam('value', $value);
                    $stmt->execute();
                }

                $this->commit();
            } catch(\Exception $e) {
                $this->rollback();
                throw $e;
            }
        }
    }

?>