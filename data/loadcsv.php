<?php

    $SEPARATOR = ';';
    $file = fopen("awt.csv", "r");

    if ($file === null)
        throw new Exception("Unable to open file");

    $keys = fgets($file);
    $keys = explode($SEPARATOR, $keys);
    foreach ($keys as &$key)
        $key = trim($key);

//    print_r($keys);
//    echo "<br><br>";

    $sqls = array();
    $id = 1;
    while ($line = utf8_decode(fgets($file))) {
        $items = explode($SEPARATOR, $line);

        for ($i = 0; $i < count($items); ++$i ) {
            $value = trim($items[$i]);
            if ( empty($value) )
                continue;


            $value = str_replace("'", "\\'", $value);
            $sqls[] = "INSERT INTO target_parameters(id, name, content) VALUES($id, '${keys[$i]}', '$value');";
        }

        ++$id;
    }

    foreach ($sqls as $sql) {
        echo($sql . "\n");
    }

?>