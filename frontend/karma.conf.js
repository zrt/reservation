module.exports = function(config){
  config.set({

    basePath : './',

    files : [
      'src/bower_components/angular/angular.js',
      'src/bower_components/angular-route/angular-route.js',
      'src/bower_components/angular-mocks/angular-mocks.js',
      'src/bower_components/angular-cookies/angular-cookies.js',
      'src/bower_components/angular-ui-bootstrap-bower/ui-bootstrap-tpls.js',
      'src/bower_components/angular-base64/angular-base64.js',
      'src/components/**/*.js',
      'src/view*/**/*.js',
      'src/view*/**/*.html'
    ],

    autoWatch : true,

    frameworks: ['jasmine'],

    browsers : ['Chrome'],

    plugins : [
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-jasmine',
            'karma-junit-reporter',
            'karma-ng-html2js-preprocessor'
            ],

    junitReporter : {
      outputFile: 'test_out/unit.xml',
      suite: 'unit'
    },

    preprocessors: {
      'src/view*/**/*.html': ["ng-html2js"]
    },

    ngHtml2JsPreprocessor: {
        // If your build process changes the path to your templates,
        // use stripPrefix and prependPrefix to adjust it.
        //stripPrefix: "source/path/to/templates/.*/",
        stripPrefix: 'src/'
        //prependPrefix: "web/path/to/templates/",

        // the name of the Angular module to create
//        moduleName: "my.templates"
    }

  });
};
