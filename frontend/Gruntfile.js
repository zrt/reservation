module.exports = function(grunt) {

  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),

    copy: {
      dist: {
        files: [
          {expand: true,  cwd:'src/', src: ['views/**/*.html'], dest: '<%= pkg.dist %>/', filter: 'isFile'},
          {expand: true,  cwd:'src/', src: ['*.css'], dest: '<%= pkg.dist %>/', filter: 'isFile'},
          {expand: true,  cwd:'src/', src: ['*.html'], dest: '<%= pkg.dist %>/', filter: 'isFile'},
          {expand: true,  cwd:'src/', src: ['api/*'], dest: '<%= pkg.dist %>/', dot: true},
          {expand: true,
            dest: '<%= pkg.dist %>/',
            cwd: 'src/',
            src: [
              'bower_components/bootstrap/dist/css/bootstrap.min.css',
              'bower_components/html5-boilerplate/dist/css/normalize.css',
              'bower_components/html5-boilerplate/dist/css/main.css',
              'bower_components/angular/angular.js',
              'bower_components/angular-route/angular-route.js',
              'bower_components/angular-cookies/angular-cookies.min.js',
              'bower_components/angular-ui-bootstrap-bower/ui-bootstrap-tpls.min.js',
              'bower_components/html5-boilerplate/dist/js/vendor/modernizr-2.8.3.min.js',
              'bower_components/angular-base64/angular-base64.js'
          ]}
        ],
      },
    },

    useminPrepare: {
      html: 'src/index.html',
      options: {
        root: 'src',
        dest: '<%= pkg.dist %>',
        flow: {
            steps: {
              js: ['concat']
            },
            post: {}
        }
      }
    },

    usemin: {
      html: '<%= pkg.dist %>/index.html',
    },

    replace: {
      dist: {
        src: ['<%= pkg.dist %>/api/index.php'],
        dest: '<%= pkg.dist %>/api/index.php',
        replacements: [{
          from:/include.*".*"/g,
          to: 'include "../../../backend/src/index.php"'
        }]
      }
    }

  });


  grunt.loadNpmTasks('grunt-usemin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-text-replace');

  grunt.registerTask('default', [
    'copy:dist',
    'useminPrepare',
    'concat:generated',
    'usemin',
    'replace'
  ]);

};