'use strict';

describe('myApp.targetsView module', function() {
    var viewUrl = 'views/targets/targets.html';
    var element, $scope, tbody;

    beforeEach(angular.mock.module('myApp.targetsView'));
    beforeEach(angular.mock.module(viewUrl));

    beforeEach(inject(function($rootScope, $compile, $templateCache) {
        $scope = $rootScope;
        element = $compile($templateCache.get(viewUrl))($scope);
        tbody = element.find('tbody');
      }));

    describe('for a simple table', function() {

        beforeEach(function() {
            $scope.meta = [
                {
                    key: 'column1',
                    name: 'Column 1',
                    display: 'value',
                    order: 0,
                    filter: false
                },
            ];
            $scope.targets = [
                { column1: 'value1' },
                { column1: 'value2' }
            ];
            $scope.$digest();
        });

        it('should create headers', function() {
            var headers = element.find('th');
            expect(headers.length).toBe(1);
            expect(headers.text()).toBe('Column 1');
        });

        it('should create rows', function() {
            var firstRow = tbody.find('tr').find('td');
            expect(firstRow.length).toBe(2);
            expect(firstRow.eq(0).text().trim()).toBe('value1');
            expect(firstRow.eq(1).text().trim()).toBe('value2');
        });

    })

    describe('filtering', function() {
        function setFilter(ndx, value) {
            element.find('input').eq(ndx).val(value).triggerHandler('input');
            $scope.$apply();
        }

        function cell(row, col) {
            return tbody.find('tr').eq(row).find('td').eq(col);
        }

        beforeEach(function() {
            $scope.meta = [
                { key: 'c1', name: 'C1', display: 'value', order: 0, filter: false },
                { key: 'c2', name: 'C2', display: 'value', order: 1, filter: true },
                { key: 'c3', name: 'C3', display: 'value', order: 2, filter: true },
            ];
            $scope.targets = [
                { c1: 'r1c1', c2: 'r1c2', c3: 'r1c3' },
                { c1: 'r2c1', c2: '',     c3: 'r2c3' },
                { c1: 'r3c1', c2: 'r3c2', c3: 'r3c3' }
            ];
            $scope.$digest();

            expect(tbody.find('tr').length).toBe(3);
            expect(tbody.find('td').length).toBe(9);
        });

        it('should add input for each filtered meta', function() {
            var inputs = element.find('th').find('input');

            expect(inputs.length).toBe(2);
            expect(inputs.eq(0).attr('placeholder')).toEqual('C2');
            expect(inputs.eq(1).attr('placeholder')).toEqual('C3');
        });

        it('should filter rows out if filter matches', function() {
            setFilter(0, 'r1');
            expect(tbody.find('tr').length).toBe(1);
            expect(cell(0,0).text().trim()).toBe('r1c1');

            setFilter(0, 'r');
            expect(tbody.find('tr').length).toBe(2);
            expect(cell(0,0).text().trim()).toBe('r1c1');
            expect(cell(1,0).text().trim()).toBe('r3c1');

            setFilter(0, 'c2');
            expect(tbody.find('tr').length).toBe(2);
            expect(cell(0,0).text().trim()).toBe('r1c1');
            expect(cell(1,0).text().trim()).toBe('r3c1');
        });

        it('should not filter empty column if filter is empty', function() {
            setFilter(0, '');
            expect(tbody.find('tr').length).toBe(3);
            expect(cell(1,0).text().trim()).toBe('r2c1');
        });

        it('should use filters combined', function() {
            setFilter(0, 'c2');
            expect(tbody.find('tr').length).toBe(2);
            expect(cell(0,0).text().trim()).toBe('r1c1');
            expect(cell(1,0).text().trim()).toBe('r3c1');

            setFilter(1, 'r1');
            expect(tbody.find('tr').length).toBe(1);
            expect(cell(0,0).text().trim()).toBe('r1c1');

            setFilter(1, 'r3');
            expect(tbody.find('tr').length).toBe(1);
            expect(cell(0,0).text().trim()).toBe('r3c1');

            setFilter(0, '');
            expect(tbody.find('tr').length).toBe(1);
            expect(cell(0,0).text().trim()).toBe('r3c1');

            setFilter(1, '');
            expect(tbody.find('tr').length).toBe(3);
        });

    });

});