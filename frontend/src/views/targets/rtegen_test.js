'use strict';

describe('rte-gen directive', function() {
    var $scope, $compile, element;

    beforeEach(angular.mock.module('myApp.targetsView'));

    beforeEach(inject(function($rootScope, _$compile_, _$timeout_) {
        $scope = $rootScope.$new();
        $compile = _$compile_;
    }));

    it('should create tag based on display key', function() {
        $scope.target = {};
        $scope.column = { display: 'value' };
        var element = $compile('<rte-gen target="target" column="column"/>')($scope);

        var rtNode = element.children(0);
        expect(rtNode.prop('tagName')).toBe('RT-VALUE');
        expect(rtNode.attr('target')).toBe('target');
        expect(rtNode.attr('column')).toBe('column');
    });

    it('should be ok for different display', function() {
        $scope.target = {};
        $scope.column = { display: 'displaygen' };
        var element = $compile('<rte-gen target="target" column="column"/>')($scope);

        var rtNode = element.children(0);
        expect(rtNode.prop('tagName')).toBe('RT-DISPLAYGEN');
    });

});
