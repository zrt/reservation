'use strict';

describe('rt-value directive', function() {
    var $scope, $compile, element;

    beforeEach(angular.mock.module('myApp.targetsView'));

    beforeEach(inject(function($rootScope, _$compile_, _$timeout_) {
        $scope = $rootScope.$new();
        $compile = _$compile_;
    }));

    function create(target, column) {
        $scope.target = target;
        $scope.column = column;

        element = $compile('<rt-value target="target" column="column"/>')($scope);

        $scope.$digest();
    }

    it('should use the name of column as key', function() {
        create({ key: 'the-value' }, { key: 'key' });
        expect(element.text()).toBe('the-value');
    });

    it('should use the name of column as key with different values', function() {
        create({ key2: 'the-value-2' }, { key: 'key2' });
        expect(element.text()).toBe('the-value-2');
    });

});
