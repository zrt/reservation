'use strict';

describe('myApp.targetsView module', function() {

    beforeEach(angular.mock.module('myApp.targetsView'));

    var controller, scope;
    var test_entries = [ {'id': 1, 'name': 'noname'}];

    var mockTargetService = jasmine.createSpyObj('TargetService', ['get', 'meta']);
    mockTargetService.get.and.returnValue([]);

    var sessionService = jasmine.createSpyObj('SessionService', ['getUserName']);

    beforeEach(function() {
        inject(function($controller, $rootScope) {
            scope = $rootScope.$new();
            controller = $controller('TargetsCtrl', {
                $scope: scope,
                Targets: mockTargetService,
                Session: sessionService
            });
        });
    });

    it('updates scope.target for targets.entities.updated event ....', function() {
        expect(scope.targets).toEqual([]);
        mockTargetService.get.and.returnValue(test_entries);
        scope.$broadcast('targets.entries.updated');
        expect(scope.targets).toEqual(test_entries);
    });

    describe('updates scope.meta for targets.meta.updated event', function() {

        it('adds two columns', function() {
            expect(scope.meta).toEqual([]);
            var meta = [ { display: 'value' }, { display: 'value' } ];
            mockTargetService.meta.and.returnValue(meta);

            scope.$broadcast('targets.meta.prepocessed.updated');
            expect(scope.meta).toEqual(meta);
        });

        it('accepts display type value, edit and lock. Nothing else.', function() {
            var meta = [ { display: 'value' }, { display: 'lock' }, { display: 'edit' }, { display: 'nonok' } ];
            mockTargetService.meta.and.returnValue(meta);

            scope.$broadcast('targets.meta.prepocessed.updated');
            expect(scope.meta).toEqual([ { display: 'value' }, { display: 'lock' }, { display: 'edit' } ]);
        });

    });

});