'use strict';

angular.module('myApp.targetsView', ['ngRoute', 'myApp.targets', 'myApp.session', 'myApp.editDialog'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/targets', {
    templateUrl: 'views/targets/targets.html',
    controller: 'TargetsCtrl'
  });
}])

.controller('TargetsCtrl', function($scope, Targets, Session) {
    $scope.targets = Targets.get();
    $scope.meta = [];
    $scope.filters = {};

    function update_meta() {
        var meta = [];

        // select columns which can be shown
        var targetMeta = Targets.meta();
        for(var key in targetMeta) {
            if (targetMeta[key].display == 'value' ||
                targetMeta[key].display == 'lock' ||
                targetMeta[key].display == 'edit')
            {
                meta.push(targetMeta[key]);
            }
        }

        $scope.meta = meta;
    }

    $scope.$on("targets.entries.updated", function() {
         $scope.targets = Targets.get();
    });

    $scope.$on("targets.meta.prepocessed.updated", function() {
        update_meta();
    });

    $scope.$watchCollection('filters', function() {
        for(var o in $scope.filters) {
            if ( !$scope.filters[o] )
                $scope.filters[o] = undefined;
        }
    });

    update_meta();
})

.directive('rteHeader', function($compile) {

    function link(scope, element, attrs) {

        var template = scope.column.name;
        if ( scope.column.filter ) {
            template = '<input type="text" ng-model="$parent.filters[column.key]" placeholder="{{column.name}}">';
        }

        element.html(template);
        $compile(element.contents())(scope);
    };

    return {
        restrict: 'E',
        link: link
    };
})

.directive('rtEdit', function($uibModal) {
    return {
        restrict: 'E',
        scope: {
            target: '=target',
            column: '=column'
        },
        template: '<input type="button" class="btn btn-warning btn-xs" value="Edit" ng-click="open(target.id)" />',
        controller: ['$scope', function($scope) {
            $scope.open = function(id) {
                var modalInstance = $uibModal.open({
                    templateUrl: 'views/edit/edit.html',
                    controller: 'EditCtrl',
                    resolve: {
                        target: function () { return $scope.target; }
                    }
                });
            }
        }]
    };
})

.directive('rtValue', function() {
    return {
        restrict: 'E',
        scope: { target: '=target', column: '=column' },
        template: '{{target[column.key]}}'
    };
})

.directive('rteGen', function($compile) {

    var linker = function(scope, element, attrs) {
        element.html('<rt-' + scope.column.display + ' target="target" column="column"/>');
        $compile(element.contents())(scope);
    };

    return {
        restrict: 'E',
        scope: {
            target: '=target',
            column: '=column'
        },
        link: linker
    };
});

