'use strict';

describe('myApp.profile module', function() {

    var viewUrl = 'views/profile/profile.html';

    beforeEach(angular.mock.module('myApp.profileDialog'));
    beforeEach(angular.mock.module(viewUrl));

    var controller, $scope, users, session;


    beforeEach(function() {
        inject(function($controller, $rootScope, $q) {

            session = jasmine.createSpyObj('Session', ['getUserName']);
            session.getUserName.and.returnValue("user");

            users = jasmine.createSpyObj('Users', ['get', 'put']);
            users.promises = {
                get: $q.defer(),
                put: $q.defer()
            }
            users.get.and.returnValue(users.promises.get.promise);
            users.put.and.returnValue(users.promises.put.promise);

            var modalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);

            $scope = $rootScope.$new();
            controller = $controller('ProfileCtrl', {
                $scope: $scope,
                Session: session,
                Users: users,
                $uibModalInstance: modalInstance,
            });
        });
    });

    describe('dialog template', function() {
        var element;

        beforeEach(inject(function($compile, $templateCache) {
            element = $compile($templateCache.get(viewUrl))($scope);
            $scope.email = 'dummy-email';
            $scope.$digest();
        }));

        it('has email field', function() {
            var inputs = element.find('input');
            expect(inputs.eq(0).attr('name')).toBe('email');
            expect(inputs.eq(0).val()).toBe('dummy-email');
        });
    });

    describe('scope', function() {
        beforeEach(function() {
            users.promises.get.resolve({ email: '' });
            $scope.$apply();
        });

        it('has username field', function() {
            expect($scope.username).toBe('user');
        });

        it('has email field', function() {
            expect($scope.email).toBe('');
        });

        it('ok calls update', function() {
            $scope.email = "set-email";
            $scope.ok();
            expect(users.put).toHaveBeenCalledWith('user', { email: 'set-email'});
        });

        it('cancel does not call update', function() {
            $scope.email = 'should-not-be-set';
            $scope.cancel();
            expect(users.put).not.toHaveBeenCalled();
        });
    });

});