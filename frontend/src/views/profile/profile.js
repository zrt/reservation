'use strict';

angular.module('myApp.profileDialog', ['myApp.session', 'myApp.users'])


.controller('ProfileCtrl', function($scope, $uibModalInstance, Session, Users) {

    $scope.username = Session.getUserName();
    Users.get($scope.username).then(function(user) {
        $scope.email = user.email;
    });

    $scope.ok = function () {
        Users.put($scope.username, { email: $scope.email }).then(function() {
            $uibModalInstance.close('ok');
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

});

