'use strict';

var module = angular.module('myApp.login', ['ui.bootstrap.modal', 'myApp.session']);


module.controller('LoginCtrl', function($scope, Session, $uibModal) {

    function updateScope() {
        $scope.username = Session.getUserName() || "";
        $scope.isLoggedIn = Session.isLoggedIn();
    }

    updateScope();
    $scope.$on('session.updated', function() {
        updateScope();
    });

    $scope.login = function(name) {
        Session.login(name);
    }

    $scope.logout = function() {
        Session.logout();
    }

    $scope.showProfileDialog = function() {
        var modalInstance = $uibModal.open({
            templateUrl: 'views/profile/profile.html',
            controller: 'ProfileCtrl'
        });
    }
});