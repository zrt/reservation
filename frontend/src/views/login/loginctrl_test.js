'use strict';

describe('login control', function() {

    beforeEach(angular.mock.module('myApp.login'));

    var loginCtrl, scope, username;

    var sessionMock = jasmine.createSpyObj('Session', ['getUserName', 'isLoggedIn', 'login', 'logout']);
    sessionMock.login.and.callFake(function(username) {
        sessionMock.getUserName.and.returnValue(username);
        sessionMock.isLoggedIn.and.returnValue(true);
        scope.$broadcast('session.updated');
    });

    sessionMock.logout.and.callFake(function() {
        sessionMock.getUserName.and.returnValue(undefined);
        sessionMock.isLoggedIn.and.returnValue(false);
        scope.$broadcast('session.updated');
    });

    beforeEach(function() {
        inject(function($controller, $rootScope) {
            scope = $rootScope.$new();
            loginCtrl = $controller('LoginCtrl', {
                $scope: scope,
                $cookies: {},
                Session: sessionMock
            });
        });
    });

    describe('scope.username', function() {
        it('changes when user logs in/out', function() {
            sessionMock.login('bela');
            scope.$apply();
            expect(scope.username).toBe("bela");

            sessionMock.logout();
            scope.$apply();
            expect(scope.username).toBe("");
        });
    });

    describe('scope has methods', function() {
        it('has login', function() {
            scope.login("bela");
            expect(sessionMock.login).toHaveBeenCalled();
        });

        it('has logout', function() {
            scope.logout();
            expect(sessionMock.logout).toHaveBeenCalled();
        });
    });

});