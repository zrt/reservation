'use strict';

describe('myApp.targetsView module', function() {

    var viewUrl = 'views/edit/edit.html';

    beforeEach(angular.mock.module('myApp.editDialog'));
    beforeEach(angular.mock.module(viewUrl));

    var controller, $scope;

    var meta = {
        column1: { display: 'value', key: 'column1', name: 'Column 1', order: 2},
        column2: { display: 'value', key: 'column2', name: 'Column 2', order: 1},
        column3: { display: 'anything', key: 'column3', name: 'Column 3', order: 3},
    };

    var target = {
        id: 2,
        column1: 'cv1',
        column2: 'cv2',
        column3: 'cv3'
    };

    var targetService = jasmine.createSpyObj('TargetService', ['meta', 'update']);
    targetService.meta.and.returnValue(meta);

    var modalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);

    beforeEach(function() {
        targetService.update.calls.reset();

        inject(function($controller, $rootScope) {
            $scope = $rootScope.$new();
            controller = $controller('EditCtrl', {
                $scope: $scope,
                Targets: targetService,
                $uibModalInstance: modalInstance,
                target: target
            });
        });
    });

    describe('dialog template', function() {
        var element;

        beforeEach(inject(function($compile, $templateCache) {
            element = $compile($templateCache.get(viewUrl))($scope);
            $scope.$digest();
        }));

        it('has value input fields with filled values and ordered ', function() {
            var inputs = element.find('input');
            expect(inputs.length).toBe(2);
            expect(inputs.eq(0).attr('type')).toBe('value');
            expect(inputs.eq(1).attr('type')).toBe('value');

            expect(inputs.eq(0).val()).toBe('cv2');
            expect(inputs.eq(1).val()).toBe('cv1');
        });

        it('has labels with name ordered', function() {
            var labels = element.find('label');
            expect(labels.length).toBe(2);
            expect(labels.eq(0).text()).toBe('Column 2');
            expect(labels.eq(1).text()).toBe('Column 1');
        });
    });

    it('ok calls update', function() {
        $scope.ok();
        expect(targetService.update).toHaveBeenCalledWith($scope.target);
    });

    it('cancel does not call update', function() {
        $scope.cancel();
        expect(targetService.update).not.toHaveBeenCalled();
    });

});