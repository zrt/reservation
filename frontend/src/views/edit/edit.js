'use strict';

angular.module('myApp.editDialog', ['ui.bootstrap.modal', 'myApp.targets'])


.controller('EditCtrl', function($scope, $uibModalInstance, Targets, target) {

    $scope.target = target;

    var meta = Targets.meta();
    var fields = [];
    for (var key in meta) {
        if ( meta[key].display == 'value' ) {
            fields.push(meta[key]);
        }
    }
    $scope.fields = fields;


    $scope.ok = function () {
        Targets.update($scope.target);
        $uibModalInstance.close('ok');
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

});

