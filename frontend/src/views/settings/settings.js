'use strict';

angular.module('myApp.settingsView', ['ngRoute', 'myApp.targets'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/settings', {
    templateUrl: 'views/settings/settings.html',
    controller: 'SettingsCtrl'
  });
}])

.controller('SettingsEntriesCtrl', function($scope, $window, Targets) {
    $scope.targets = {};
    $scope.newEntries = [];

    updateEntries();

    $scope.$on("targets.entries.updated", function() {
        updateEntries();
    });


    function updateEntries() {
         var targets = Targets.get();
         var indices = [];
         for(var ndx in targets) {
            indices.push("" + targets[ndx].id);

            var obj = angular.copy(targets[ndx]);
            delete(obj.id);

            var original = $scope.targets[targets[ndx].id];
            var str = JSON.stringify(obj);

            // not exists yet, just create the initial object for the view
            if ($scope.targets[targets[ndx].id] === undefined) {
                $scope.targets[targets[ndx].id] = { str: str };
            } else if (original.str != str && !original.changed) {
                // do not change if row was changed by user
                original.str = str;
            }
         }

         // delete all entries which are not in the targets.get
         for(var key in $scope.targets) {
            if (indices.indexOf(key) === -1)
                delete($scope.targets[key]);
         }
    }

    $scope.save = function(key, row) {
        console.log("Save " + key);
        var obj;
        try {
            obj = JSON.parse(row.str);
        } catch(e) {
            console.log(e);
            return;
        }

        obj.id = key;
        row.changed = false;
        Targets.update(obj);
    }

    $scope.delete = function(key) {
        var isOk = $window.confirm('Are you sure you want to delete entry id ' + key + '?');
        if (!isOk)
            return;
        Targets.delete(key);
    }

    $scope.deleteNew = function(key) {
        var isOk = true;
        if ($scope.newEntries[key].changed) {
            isOk = $window.confirm('Are you sure you want to loose that entry?');
        }
        if (!isOk)
            return;

        $scope.newEntries.splice(key, 1);
    }

    $scope.addNew = function() {
        $scope.newEntries.push({
            str: "{}",
            changed: false
        });
    }

    $scope.post = function(row) {
        var obj;
        try {
            obj = JSON.parse(row.str);
        } catch(e) {
            console.log(e);
            return;
        }

        Targets.post(obj)
        .then(function() {
            for(var ndx in $scope.newEntries) {
                if (row.str == $scope.newEntries[ndx].str) {
                    $scope.newEntries.splice(ndx, 1);
                    return;
                }
            }
        });
    }

    $scope.valueChanged = function(row) {
        try {
            var parsed = JSON.parse(row.str);
            row.jsonError = false
            console.log("no error");
        } catch(e) {
            row.jsonError = e;
            console.log(e);
        }

        row.changed = true;
    }
})

.controller('SettingsCtrl', function($scope, Targets) {
    $scope.supportedParameterNames = ['filter', 'display', 'zones.view', 'order', 'name', 'type'];
    $scope.meta = [];
    $scope.metaSaveInProgress = false;

    var objEquals = function(a) {
        return "" + a.original == "" + a.actual;
    }

    function valueObj(original, actual) {
        return {
            original: original,
            actual: actual
        }
    }

    var emptyMetaRow = {
                key: valueObj('', ''),
                value: valueObj('', '')
            };

    function findMetaByName(name) {
        for(var ndx in $scope.meta)
            if ( $scope.meta[ndx].key.original == name )
                return $scope.meta[ndx];
        return null;
    }

    function findRowByKey(meta, key) {
        for(var ndx in meta.columns)
            if ( meta.columns[ndx].key.original == key )
                return meta.columns[ndx];
        return null;
    }

    function findRowByKeyByActual(meta, key) {
        for(var ndx in meta.columns) {
            if ( meta.columns[ndx].key.actual == key )
                return meta.columns[ndx];
        }
        return null;
    }

    function checkForEmpties() {
        for (var ndx in $scope.meta) {
            for (var ki = $scope.meta[ndx].columns.length - 1; ki >= 0; ki--) {
                var row = $scope.meta[ndx].columns[ki];
                // remove empties and add a new one to the end
                if (row.key.original == "" && row.key.actual == "" && row.value.actual == "" && row.value.original == "")
                    $scope.meta[ndx].columns.splice(ki, 1);
            }
            $scope.meta[ndx].columns.push(angular.copy(emptyMetaRow));
        }
    }

    function metaUpdate() {
        var rawMeta = Targets.rawMeta();

        var foundMeta = [];
        for (var column in rawMeta) {

            foundMeta.push(column);

            var meta = findMetaByName(column);
            if ( !meta ) {
                meta = {
                    key: valueObj(column, column),
                    columns: []
                };
                $scope.meta.push(meta);
            }

            var foundKeys = [];
            for (var metaKey in rawMeta[column]) {
                if ( metaKey == 'key' ) // skip the 'key' key
                    continue;

                foundKeys.push(metaKey);

                var row = findRowByKey(meta, metaKey);
                if ( !row ) {
                    row = {
                        key: valueObj(metaKey, metaKey),
                        value: valueObj(rawMeta[column][metaKey], rawMeta[column][metaKey]),
                    };
                    meta.columns.push(row);
                } else {
                    row.key.original = metaKey;
                    row.value.original = rawMeta[column][metaKey];
                }
            }

            // remove elements which were not in the new dataset
            for (var ndx = meta.columns.length - 1; ndx >= 0; --ndx) {
                var rowKey = meta.columns[ndx].key;
                if (rowKey.original != "" && -1 == foundKeys.indexOf(rowKey.original))
                    meta.columns.splice(ndx, 1);
            }
        }

        // remove metas which no longer arrives
        for (var ndx = $scope.meta.length - 1; ndx >= 0; --ndx) {
            var metaKey = $scope.meta[ndx].key;
            if (metaKey.original != "" && -1 == foundMeta.indexOf($scope.meta[ndx].key.original))
                $scope.meta.splice(ndx, 1);
        }

        checkForEmpties();
    }

    $scope.metaColumnAdd = function() {
        $scope.meta.push({
            key: valueObj('', ''),
            columns: [ angular.copy(emptyMetaRow) ]
        });
    }

    $scope.metaColumnUndo = function(column) {
        column.key.actual = column.key.original;
        column.toDelete = false;
    }

    $scope.metaUndo = function(row) {
        row.key.actual = row.key.original;
        row.value.actual = row.value.original;
        row.toDelete = false;
    }

    $scope.metaSave = function(row) {
        $scope.metaSaveInProgress = true;

        var actions = [];
        function addAction(cmd, params) {
            actions.push({cmd: cmd, params:params});
        }

        for (var c_ndx in $scope.meta) {
            var col = $scope.meta[c_ndx];

            if (col.toDelete) {
                addAction('DELETEKEY', {meta: col.key.original});
                continue;
            }

            if (col.key.original != "" && !objEquals(col.key)) {
                addAction('RENAMEKEY', {meta: col.key.original, to: col.key.actual});
            }

            // assign original the new value
            col.key.original = col.key.actual;

            var post_values = {};
            var has_post_values = false;
            for (var r_ndx in col.columns) {
                var row = col.columns[r_ndx];

                if (row.key.original != "" && (row.toDelete || row.key.actual == "" || !objEquals(row.key))) {
                    addAction('DELETE', {meta: col.key.actual, key: row.key.original});
                }

                if (row.toDelete || !row.key.actual)
                    continue;

                // POST in case key renamed or value changed (or PUT?)
                if (!objEquals(row.key) || !objEquals(row.value)) {
                    post_values[row.key.actual] = row.value.actual;
                    has_post_values = true;

                    // change original so update will update these
                    row.key.original = row.key.actual;
                    row.value.original = row.value.actual;
                }
            }
            if ( has_post_values )
                addAction('POST', {meta: col.key.actual, values: post_values});
        }

        function call() {
            if (actions.length == 0) {
                $scope.metaSaveInProgress = false;
                metaUpdate();
                return;
            }

            var action = actions.shift();
            var a;
            // todo replace this with function ptr's
            switch (action.cmd) {
                case 'DELETEKEY':
                    a = Targets.metaDeleteKey(action.params.meta);
                    break;
                case 'RENAMEKEY':
                    a = Targets.metaRenameKey(action.params.meta, action.params.to);
                    break;
                case 'DELETE':
                    a = Targets.metaDelete(action.params.meta, action.params.key);
                    break;
                case 'POST':
                    a = Targets.metaPost(action.params.meta, action.params.values);
                    break;
            }
            a.then(function(ok) { call(); }, function() { alert('Some requests failed. Please reload'); });
        };

        call();
    }

    $scope.metaRowChanged = function(row) {
        checkForEmpties();
    }


    $scope.objEquals = objEquals;

    $scope.$on("targets.meta.raw.updated", function() {
        if ( $scope.metaSaveInProgress )
            return;
        metaUpdate();
    });

    metaUpdate();
});
