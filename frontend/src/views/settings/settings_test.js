'use strict';

describe('myApp.settingsView module', function() {

    beforeEach(angular.mock.module('myApp.settingsView'));

    var defaultMeta = {
        key1: {
            mk1: 'k1mk1',
            mk2: 'k1mk2',
            mk3: 'k1mk3'
        },
        key2: {
            mk1: 'k2mk1',
            mk4: 'k2mk4'
        },
        key3: {
            mk3: true,
            mk4: ''
        }
    };

    var $scope, controller, targetService;

    function valueObj(original, actual) {
        return { original: original, actual: actual };
    }

    beforeEach(function() {
        var promise = { then: function(a) { a(); } };
        targetService = jasmine.createSpyObj('TargetService', ['rawMeta', 'metaDeleteKey', 'metaRenameKey', 'metaDelete', 'metaPost' ]);
        targetService.rawMeta.and.returnValue(defaultMeta);
        targetService.metaDeleteKey.and.returnValue(promise);
        targetService.metaRenameKey.and.returnValue(promise);
        targetService.metaDelete.and.returnValue(promise);
        targetService.metaPost.and.returnValue(promise);

        inject(function($controller, $rootScope) {
            $scope = $rootScope.$new();
            controller = $controller('SettingsCtrl', {
                $scope: $scope,
                Targets: targetService
            });
        });
    });

    describe('scope.objEquals', function() {
        it('returns true when equal', function() {
            expect($scope.objEquals(valueObj('abc', 'abc'))).toBe(true);
            expect($scope.objEquals(valueObj('', ''))).toBe(true);
            expect($scope.objEquals(valueObj(true, true))).toBe(true);
            expect($scope.objEquals(valueObj(false, false))).toBe(true);
        });

        it('returns false when different', function() {
            expect($scope.objEquals(valueObj('abc', ''))).toBe(false);
            expect($scope.objEquals(valueObj('', 'abc'))).toBe(false);
            expect($scope.objEquals(valueObj('a', 'b'), true)).toBe(false);
            expect($scope.objEquals(valueObj(true, false))).toBe(false);
        })
    });

    describe('meta changes', function() {
        it('fills params for meta table', function() {
            expect($scope.meta[0].key).toEqual(valueObj('key1', 'key1'));
            expect($scope.meta[0].columns[0]).toEqual({ key: valueObj('mk1', 'mk1'), value: valueObj('k1mk1', 'k1mk1') });
            expect($scope.meta[0].columns[1]).toEqual({ key: valueObj('mk2', 'mk2'), value: valueObj('k1mk2', 'k1mk2') });
            expect($scope.meta[0].columns[2]).toEqual({ key: valueObj('mk3', 'mk3'), value: valueObj('k1mk3', 'k1mk3') });

            expect($scope.meta[1].key).toEqual(valueObj('key2', 'key2'));
            expect($scope.meta[1].columns[0]).toEqual({ key: valueObj('mk1', 'mk1'), value: valueObj('k2mk1', 'k2mk1') });
            expect($scope.meta[1].columns[1]).toEqual({ key: valueObj('mk4', 'mk4'), value: valueObj('k2mk4', 'k2mk4') });
        })
    });

    describe('meta save', function() {
        it('calls targetService.metaRenameKey when key changed', function() {
            $scope.meta[0].key.actual = "key1m";
            $scope.metaSave();
            expect(targetService.metaRenameKey).toHaveBeenCalledWith('key1', 'key1m');
        });

        it('calls targetService.metaDeleteKey when key deleted', function() {
            $scope.meta[0].toDelete = true;
            $scope.metaSave();
            expect(targetService.metaDeleteKey).toHaveBeenCalledWith('key1');
        });

        it('calls targetService.metaDelete when metas are deleted', function() {
            $scope.meta[0].columns[1].toDelete = true;
            $scope.meta[0].columns[2].toDelete = true;
            $scope.metaSave();
            expect(targetService.metaDelete).toHaveBeenCalledWith('key1', 'mk2');
            expect(targetService.metaDelete).toHaveBeenCalledWith('key1', 'mk3');
        });

        it('calls targetService.metaPost when meta values are changed', function() {
            $scope.meta[0].columns[1].value.actual = 'abc';
            $scope.meta[0].columns[2].value.actual = 'def';
            $scope.meta[1].columns[0].value.actual = 'ghi';
            $scope.metaSave();
            expect(targetService.metaPost).toHaveBeenCalledWith('key1', {mk2: 'abc', mk3: 'def'});
            expect(targetService.metaPost).toHaveBeenCalledWith('key2', {mk1: 'ghi'});
        });

        it('calls targetService.metaDelete and post when meta key is changed', function() {
            $scope.meta[0].columns[1].key.actual = 'def';
            $scope.metaSave();
            expect(targetService.metaDelete).toHaveBeenCalledWith('key1', 'mk2');
            expect(targetService.metaPost).toHaveBeenCalledWith('key1', {def: 'k1mk2'});
        });

        it('calls post with renamed key', function() {
            $scope.meta[0].key.actual = 'newkey';
            $scope.meta[0].columns[0].value.actual = 'abc';
            $scope.metaSave();
            expect(targetService.metaRenameKey).toHaveBeenCalledWith('key1', 'newkey');
            expect(targetService.metaPost).toHaveBeenCalledWith('newkey', {mk1: 'abc'});
        });

        it('calls requests synchronously', function() {
            $scope.meta[0].key.actual = 'newkey';
            $scope.meta[0].columns[0].value.actual = 'abc';
            targetService.metaRenameKey.and.returnValue({ then: function(a) {
                expect(targetService.metaPost).not.toHaveBeenCalled();
                a();
            } });
            $scope.metaSave();
            expect(targetService.metaRenameKey).toHaveBeenCalledWith('key1', 'newkey');
            // it is called at the end, but not in the callback
            expect(targetService.metaPost).toHaveBeenCalledWith('newkey', {mk1: 'abc'});
        });

        it('changes the empty parameter originals to actual so newly entered parameters does not show up twice after an update', function() {
            $scope.meta[0].columns[3].key.actual = 'def';
            $scope.meta[0].columns[3].value.actual = 'ghi';
            var data = angular.copy(defaultMeta);
            data.key1.def = 'ghi';
            targetService.rawMeta.and.returnValue(data);
            $scope.metaSave();
            expect($scope.meta[0].columns[3]).toEqual({ key: valueObj('def', 'def'), value: valueObj('ghi', 'ghi') });
        });

        it('changes the empty original to actual for meta keys as well', function() {
            $scope.metaColumnAdd();
            $scope.meta[3].key.actual = 'newkey';
            $scope.meta[3].columns[0].key.actual = 'a';
            $scope.meta[3].columns[0].value.actual = 'b';
            var data = angular.copy(defaultMeta);
            data.newkey = { a: 'b' };
            targetService.rawMeta.and.returnValue(data);

            $scope.metaSave();
            expect($scope.meta.length).toBe(4);
            expect($scope.meta[3].columns[0]).toEqual({ key: valueObj('a', 'a'), value: valueObj('b', 'b') });
        });
    });

    describe('empty rows', function() {
        it('adds empty rows', function() {
            expect($scope.meta[0].columns[3]).toEqual({ key: valueObj('', ''), value: valueObj('', '') });
            expect($scope.meta[1].columns[2]).toEqual({ key: valueObj('', ''), value: valueObj('', '') });
            expect($scope.meta[2].columns[2]).toEqual({ key: valueObj('', ''), value: valueObj('', '') });
        });

        it('adds new row when data entered into the existing empty', function() {
            $scope.meta[0].columns[3].key.actual = 'newkey';
            $scope.metaRowChanged();
            expect($scope.meta[0].columns[4]).toEqual({ key: valueObj('', ''), value: valueObj('', '') });
        });

        it('removes the empty row if there is two', function() {
            $scope.meta[0].columns[3].key.actual = 'newkey';
            $scope.metaRowChanged();
            $scope.meta[0].columns[3].key.actual = '';
            $scope.metaRowChanged();
            expect($scope.meta[0].columns.length).toBe(4);
        });
    });

    describe('meta update', function() {
        function clone(obj) {
            if (null == obj || "object" != typeof obj) return obj;
            var copy = obj.constructor();
            for (var attr in obj) {
                if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
            }
            return copy;
        }

        var data;
        beforeEach(function() {
            data = clone(defaultMeta);
            targetService.rawMeta.and.returnValue(data);
        });

        it('changes the original value', function() {
            data.key1.mk1 = 'value';
            $scope.$broadcast('targets.meta.raw.updated');
            expect($scope.meta[0].columns[0].value.original).toBe('value');
        });

        it('adds element if new key arrives', function() {
            data.key1.mk4 = 'val';
            $scope.$broadcast('targets.meta.raw.updated');
            expect($scope.meta[0].columns[3].key.original).toBe('mk4');
            expect($scope.meta[0].columns[3].value.original).toBe('val');
        });

        it('removes key it no longer arrives (first)', function() {
            delete(data.key1.mk1);
            $scope.$broadcast('targets.meta.raw.updated');
            expect($scope.meta[0].columns.length).toBe(3);
            expect($scope.meta[0].columns[0].key.original).toBe('mk2');
        });

        it('removes key it no longer arrives', function() {
            delete(data.key1.mk2);
            delete(data.key1.mk3);
            $scope.$broadcast('targets.meta.raw.updated');
            expect($scope.meta[0].columns.length).toBe(1 + 1);
            expect($scope.meta[0].columns[0].key.original).toBe('mk1');
        });

        it('adds meta if new arrives', function() {
            data.key4 = { mk1: 'val' };
            $scope.$broadcast('targets.meta.raw.updated');
            expect($scope.meta[3].key.original).toBe('key4');
            expect($scope.meta[3].key.actual).toBe('key4');
            expect($scope.meta[3].columns[0].key.original).toBe('mk1');
            expect($scope.meta[3].columns[0].key.actual).toBe('mk1');
            expect($scope.meta[3].columns[0].value.original).toBe('val');
            expect($scope.meta[3].columns[0].value.actual).toBe('val');
        });

        it('removes meta if it is no longer in the dataset (first)', function() {
            delete(data.key1);
            $scope.$broadcast('targets.meta.raw.updated');
            expect($scope.meta[0].key.original).toBe('key2');
        });

        it('removes meta if it is no longer in the dataset', function() {
            delete(data.key1);
            delete(data.key2);
            $scope.$broadcast('targets.meta.raw.updated');
            expect($scope.meta.length).toBe(1);
            expect($scope.meta[0].key.original).toBe('key3');
        });

        it('does not remove keys with empty original (loading does not aborts user input)', function() {
            $scope.meta[0].columns[3].key.actual = 'newkey';
            $scope.$broadcast('targets.meta.raw.updated');
            expect($scope.meta[0].columns[3].key.original).toBe('');
            expect($scope.meta[0].columns[3].key.actual).toBe('newkey');
        });

        it('does not remove metas with empty original key', function() {
            $scope.metaColumnAdd();
            $scope.meta[3].key.actual = 'newkey';
            $scope.$broadcast('targets.meta.raw.updated');
            expect($scope.meta[3].key.original).toBe('');
            expect($scope.meta[3].key.actual).toBe('newkey');
        });
    })


    /// for entries
    describe('entries page', function() {
        var $window;

        beforeEach(function() {
            var promise = { then: function(a) { a(); } };
            targetService = jasmine.createSpyObj('TargetService', ['get', 'update', 'delete', 'post']);
            targetService.get.and.returnValue([{id: 1, key: 'v1'}, {id:"4", key:'v2'}]);
            targetService.update.and.returnValue(promise);
            targetService.delete.and.returnValue(promise);
            targetService.post.and.returnValue(promise);

            $window = jasmine.createSpyObj('$window', ['confirm']);

            inject(function($controller, $rootScope) {
                $scope = $rootScope.$new();
                controller = $controller('SettingsEntriesCtrl', {
                    $scope: $scope,
                    $window: $window,
                    Targets: targetService
                });
            });
        });

        it('sets $scope.targets and newEntries', function() {
            expect($scope.newEntries.length).toBe(0);
            expect($scope.targets).toEqual({"1":{ str: '{"key":"v1"}' }, "4":{str:'{"key":"v2"}'} });
        })

        it('adds $scope.addNew', function() {
            $scope.addNew();
            expect($scope.newEntries).toEqual([{"str":"{}", "changed": false}])
        });

        it('adds $scope.deleteNew(id)', function() {
            $scope.addNew();
            $scope.addNew();
            $scope.newEntries[0].str = "{ abc }";
            $scope.newEntries[1].str = "{ def }";
            $scope.deleteNew(1);
            expect($scope.newEntries.length).toBe(1);
            expect($scope.newEntries[0].str).toEqual("{ abc }");
        });

        it('adds $scope.post', function() {
            $scope.addNew();
            $scope.newEntries[0].str = '{"a":12, "b":"value2", "c":"abc"}';
            $scope.post($scope.newEntries[0]);
            expect(targetService.post).toHaveBeenCalledWith({"a":12, "b":"value2", "c":"abc"});
            expect($scope.newEntries.length).toBe(0);   // new entry removed, update will add the new one to targets
        });

        it('adds $scope.save', function() {
            var row = { str: '{"a":"val-a", "b":"b-val"}' };
            $scope.save(123, row);
            expect(targetService.update).toHaveBeenCalledWith({"id":123, "a":"val-a", "b":"b-val"});
            expect(row.changed).toBe(false);
        });

        it('adds $scope.delete', function() {
            $window.confirm.and.returnValue(true);
            $scope.delete(112);
            expect(targetService.delete).toHaveBeenCalledWith(112);
        });

        it('adds $scope.delete (which asks confirmation)', function() {
            $window.confirm.and.returnValue(false);
            $scope.delete(112);
            expect(targetService.delete).not.toHaveBeenCalled();
        });

    });
});