'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ui.bootstrap',
  'ngRoute',
  'myApp.targetsView',
  'myApp.settingsView',
  'myApp.login',
  'myApp.lock',
  'myApp.targets',
  'myApp.session',
  'myApp.profileDialog'
])

.constant('settings', { updateIntervalMs: 10000 })

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.otherwise({redirectTo: '/targets'});
}]);
