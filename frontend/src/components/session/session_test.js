'use strict';

describe('session service', function() {

    var session, cookies, rootScope, http, $base64;

    beforeEach(function() {
        angular.mock.module('myApp.session', function($provide) {
            cookies = jasmine.createSpyObj('$cookies', ['put', 'remove', 'get']);
            cookies.get.and.returnValue('username');
            $provide.value('$cookies', cookies);

            rootScope = jasmine.createSpyObj('$rootScope', ['$broadcast', '$destroy']);
            $provide.value('$rootScope', rootScope);
        });

        inject(function(Session, $http, _$base64_) {
            session = Session;
            http = $http;
            $base64 = _$base64_;
        });

    });

    describe('isLoggedIn', function() {
        it('returns false if not logged in', function() {
            session.logout();
            expect(session.isLoggedIn()).toBe(false);
        });

        it('returns true if logged in', function() {
            session.login("xx");
            expect(session.isLoggedIn()).toBe(true);
        });
    });

    it('getUserName returns empty string when not logged in', function() {
        session.logout();
        expect(session.getUserName()).toBe(undefined);
    });

    it('getUserName returns the username when logged in', function() {
        session.login("xxx");
        expect(session.getUserName()).toBe("xxx");
    });

    it('defines authorization header when logged in', function() {
        session.login("xx");
        expect(http.defaults.headers.common['Authorization']).toBe("basic " + $base64.encode("xx:"));
        expect(cookies.put).toHaveBeenCalledWith("login", "xx");
        expect(rootScope.$broadcast).toHaveBeenCalledWith('session.updated');
    });

    it('logout', function() {
        session.logout();
        expect(http.defaults.headers.common['Authorization']).toBeUndefined();
        expect(cookies.remove).toHaveBeenCalledWith("login");
        expect(rootScope.$broadcast).toHaveBeenCalledWith('session.updated');
    });

    describe('cookies are used for session', function() {
        it("loads user session at start", function() {
            expect(session.getUserName()).toBe("username");
        });
    });

});