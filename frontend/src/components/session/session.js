'use strict';

var module = angular.module('myApp.session', ['ngCookies', 'base64']);

module.service('Session', function($http, $cookies, $rootScope, $base64) {

    var COOKIE_NAME = "login";
    var username = undefined;
    var email = undefined;

    function initialize() {
        var username = $cookies.get(COOKIE_NAME);
        if (username)
            SessionService.login(username);
    }

    var SessionService = {

        login : function (name) {
            username = name;
            $cookies.put(COOKIE_NAME, username);
            $http.defaults.headers.common['Authorization'] = 'basic ' + $base64.encode(username + ":");
            $rootScope.$broadcast('session.updated');
        },

        logout : function() {
            username = undefined;
            $cookies.remove(COOKIE_NAME);
            $http.defaults.headers.common['Authorization'] = undefined;
            $rootScope.$broadcast('session.updated');
        },

        isLoggedIn : function() {
            return username != undefined && username.length > 0;
        },

        getUserName : function() {
            return username;
        },

    };

    initialize();

    return SessionService;
});