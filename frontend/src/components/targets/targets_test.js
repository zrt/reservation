'use strict';

describe('Target Service', function() {

    var GETReply = {
        'entries': [
            {'id': 1, 'name': 'noname'}
        ],
        'meta': {
            column1: { display: 'display', key: 'overwritten', name: 'Column 1', order: 2 },
            column2: {}
        } };
    var service, $httpBackend, $rootScope;
    var config = { updateIntervalMs: 50 };

    beforeEach(function() {

        angular.mock.module('myApp.targets');
        angular.mock.module(function($provide) { $provide.constant('settings', config); });

        inject(function(Targets, $injector) {
            service = Targets;
            $httpBackend = $injector.get('$httpBackend');
            $rootScope = $injector.get('$rootScope');
        });
    });

    afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    function checkInit() {
        $httpBackend.when('GET', 'api/index.php/targets').respond(200, { 'entries': GETReply });
        $httpBackend.flush();
    }

    describe('the service', function() {

        beforeEach(function() {
            spyOn($rootScope, '$broadcast').and.callThrough();
            $httpBackend.expectGET('api/index.php/targets').respond(200, GETReply);
            $httpBackend.flush();   // initialization part
        });

        it('calls load at initialize', function() {
            var entries = service.get();

            expect(entries).toEqual(GETReply.entries);
            expect(service.meta().column1).toBeDefined();
            expect(service.meta().column2).toBeDefined()
            expect($rootScope.$broadcast).toHaveBeenCalledWith('targets.meta.raw.updated');
            expect($rootScope.$broadcast).toHaveBeenCalledWith('targets.meta.prepocessed.updated');
            expect($rootScope.$broadcast).toHaveBeenCalledWith('targets.entries.updated');
        });

        it('calls load periodically', function(done) {
            setTimeout(function() {
                $httpBackend.expectGET('api/index.php/targets').respond(200, GETReply);
                $httpBackend.flush();
                expect($rootScope.$broadcast.calls.count()).toEqual(3 * 2);     // 2 for init, 2 for update
                done();
            }, 51);
        });

        it('reloads targets when session changes', function() {
            $rootScope.$broadcast('session.updated');
            $httpBackend.expectGET('api/index.php/targets').respond(200, GETReply);
            $httpBackend.flush();
        });

    });

    describe('the service', function() {

        it('fills meta with default values', function() {
            $httpBackend.expectGET('api/index.php/targets').respond(200, { entries: [], meta: { column1: {} }});
            $httpBackend.flush();

            expect(service.meta()).toEqual({ column1: { display: 'value', key: 'column1', name: 'column1', order: 0, filter: false } });
        });

        it('does not changes provided values', function() {
            $httpBackend.expectGET('api/index.php/targets').respond(200, { entries: [], meta: { column1: { display: 'x', name: 'y', order: 3, filter: 'yes' } }});
            $httpBackend.flush();

            expect(service.meta()).toEqual({ column1: { display: 'x', key: 'column1', name: 'y', order: 3, filter: true } });
        });


        it('converts meta.order to integer', function() {
            $httpBackend.expectGET('api/index.php/targets').respond(200, { entries: [], meta: { column1: { order: '13' } }});
            $httpBackend.flush();

            expect(service.meta().column1.order).toBe(13);
        });

        it('sets meta.order to zero if it is not an int', function() {
            $httpBackend.expectGET('api/index.php/targets').respond(200, { entries: [], meta: { column1: { order: 'abc' } }});
            $httpBackend.flush();

            expect(service.meta().column1.order).toBe(0);
        });
    });

    describe('for targets', function() {

        beforeEach(checkInit);

        it('provides update', function() {
            $httpBackend.expectPUT('api/index.php/targets/3').respond(200);

            service.update({id: 3, col: 'value'});
            $httpBackend.flush();
        });

        it('provides lock', function() {
            $httpBackend.expectPOST('api/index.php/targets/2/lock').respond(200);

            service.lock({id: 2});
            $httpBackend.flush();
        });

        it('provides unlock', function() {
            $httpBackend.expectPOST('api/index.php/targets/3/unlock').respond(200);

            service.unlock({id: 3});
            $httpBackend.flush();
        });

        it('provides delete', function() {
            $httpBackend.expectDELETE('api/index.php/targets/1').respond(200);
            service.delete(1);
            $httpBackend.flush();
        });

        it('provides post', function() {
            $httpBackend.expectPOST('api/index.php/targets/', {'key':'val', 'key2':'val2'}).respond(200);
            service.post({'key':'val', 'key2':'val2'});
            $httpBackend.flush();
        });

    });

    describe('for meta update', function() {

        beforeEach(checkInit);

        it('provides metaDeleteKey', function() {
            $httpBackend.expectDELETE('api/index.php/targets/meta/abc').respond(200);
            service.metaDeleteKey('abc');
            $httpBackend.flush();
        });

        it('provides metaRenameKey', function() {
            $httpBackend.expectPATCH('api/index.php/targets/meta/abc', {to: 'def'}).respond(200);
            service.metaRenameKey('abc', 'def');
            $httpBackend.flush();
        });

        it('provides metaDelete', function() {
            $httpBackend.expectDELETE('api/index.php/targets/meta/abc/key').respond(200);
            service.metaDelete('abc', 'key');
            $httpBackend.flush();
        });

        it('provides metaPost', function() {
            $httpBackend.expectPOST('api/index.php/targets/meta/abc', {mk: 'mkv', mk2: 'mkv2'}).respond(200);
            service.metaPost('abc', { mk: 'mkv', mk2: 'mkv2'});
            $httpBackend.flush();
        });

    });
});