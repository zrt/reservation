'use strict';

var module = angular.module('myApp.targets', ['myApp.session']);

module.service('Targets', function($http, $rootScope, settings) {

    var TARGETS_URL = 'api/index.php/targets';
    var TARGETS_META_URL = 'api/index.php/targets/meta';
    var TARGETS_UPDATE_INTERVAL_MS = settings.updateIntervalMs;

    var targets = [];
    var raw_meta = {};
    var prepocessed_meta = {};
    var targets_update_timer;

    load();

    function preprocess_meta() {
        // preprocessing to fill fields needed for other parts
        var result = {};
        var order = 0;
        for(var key in raw_meta) {
            result[key] = {
                'key': key,
                'name': raw_meta[key].name || key,
                'display': raw_meta[key].display || 'value',
                'order': parseInt(raw_meta[key].order) || order,
                'filter': raw_meta[key].filter == 'yes'
            };

            order++;
        }
        prepocessed_meta = result;
        $rootScope.$broadcast('targets.meta.prepocessed.updated');
    }

    function load() {
        clearTimeout(targets_update_timer);

        $http({
                method: 'GET',
                url: TARGETS_URL
            }).then(function(result) {
                if ( result.data && result.data.meta ) {
                    raw_meta = result.data.meta;
                    $rootScope.$broadcast('targets.meta.raw.updated');
                    preprocess_meta();
                }

                if ( result.data && result.data.entries ) {
                    targets = result.data.entries;
                    // todo? do not send message when data is same!
                    $rootScope.$broadcast('targets.entries.updated');
                }

                clearTimeout(targets_update_timer);
                targets_update_timer = setTimeout(load, TARGETS_UPDATE_INTERVAL_MS);
            });
    }

    $rootScope.$on('session.updated', function() {
        load();
    });

    return {

        get : function() {
            return targets;
        },

        meta : function() {
            return prepocessed_meta;
        },

        rawMeta : function() {
            return raw_meta;
        },

        lock: function(target) {
            return $http({
                method: 'POST',
                url: TARGETS_URL + "/" + target.id + "/lock"
            }).then(function() {
                load();
            });
        },

        unlock: function(target) {
            return $http({
                method: 'POST',
                url: TARGETS_URL + "/" + target.id + "/unlock"
            }).then(function() {
                load();
            });
        },

        update: function(target) {
            return $http({
                method: 'PUT',
                url: TARGETS_URL + "/" + target.id,
                data: target
            }).then(function() {
                load();
            })
        },

        delete: function(id) {
            return $http({
                method: 'DELETE',
                url: TARGETS_URL + "/" + id
            }).then(function() {
                load();
            })
        },

        post: function(data) {
            return $http({
                method: 'POST',
                url: TARGETS_URL + "/",
                data: data
            }).then(function() {
                load();
            })
        },

        metaDeleteKey: function(key) {
            return $http({
                method: 'DELETE',
                url: TARGETS_META_URL + "/" + key
            }).then(function() {
                load();
            })
        },

        metaRenameKey: function(key, to) {
            return $http({
                method: 'PATCH',
                url: TARGETS_META_URL + "/" + key,
                data: { to: to }
            }).then(function() {
                load();
            })
        },

        metaDelete: function(key, param) {
            return $http({
                method: 'DELETE',
                url: TARGETS_META_URL + "/" + key + "/" + param
            }).then(function() {
                load();
            })
        },

        metaPost: function(key, values) {
            return $http({
                method: 'POST',
                url: TARGETS_META_URL + "/" + key,
                data: values
            }).then(function() {
                load();
            })
        }
    }
});