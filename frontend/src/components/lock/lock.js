'use strict';

angular.module('myApp.lock', ['myApp.session', 'myApp.targets'])

.controller('LockCtrl', function($scope, Session, Targets) {

    function getState() {
        var user = Session.getUserName();
        if ( !user )
            return '';


        if ( $scope.target.locked != undefined && $scope.target.locked.length > 0 ) {
            if ( user.toUpperCase() == $scope.target.locked[0].toUpperCase() )
                return 'locked';

            for(var e in $scope.target.locked)
                if ( $scope.target.locked[e].toUpperCase() == user.toUpperCase() )
                    return 'queued';
            return 'queueable';
        }

        return 'lockable';
    }

    $scope.state = getState();

    $scope.$on('session.updated', function() {
        $scope.state = getState();
    });

    $scope.lock = function() {
        Targets.lock($scope.target);
    }

    $scope.unlock = function() {
        Targets.unlock($scope.target);
    }
})

.directive('rtLock', function() {
    return {
        restrict: 'E',
        scope: { target: '=target' },
        controller: 'LockCtrl',
        template:
            '<table><tr ng-switch on="state">' +
            '<td ng-switch-when="lockable">' +
            '    <button class="btn btn-xs btn-primary" type="button" ng-click="lock()">Lock</button>' +
            '</td>' +
            '<td ng-switch-when="locked">' +
            '    <button class="btn btn-xs btn-success" type="button" ng-click="unlock()">Unlock</button>' +
            '</td>' +
            '<td ng-switch-when="queueable">' +
            '    <button class="btn btn-xs btn-info" type="button" ng-click="lock()">Queue up</button>' +
            '</td>' +
            '<td ng-switch-when="queued">' +
            '    <button class="btn btn-xs btn-info" type="button" ng-click="unlock()">Leave queue</button>' +
            '</td>' +
            '</td>' +
            '<td>{{target.locked[0]}}</td>' +
            '</tr></table>'
    };
});
