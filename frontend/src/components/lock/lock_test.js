'use strict';

describe('lock control', function() {
    beforeEach(angular.mock.module('myApp.lock'));

    var lockCtrl, scope;

    var session = {
        username: "",
        getUserName: function() { return this.username; },
    };

    var target = {
        locked: undefined
    }

    var Targets = jasmine.createSpyObj('TargetService', ['lock', 'unlock']);

    // late instantation of the controller
    // because i want to test its initial state {{
    var do_inject = function() {
        inject(function($controller, $rootScope) {
            scope = $rootScope.$new();
            scope.target = target;
            lockCtrl = $controller('LockCtrl', {
                $scope: scope,
                Session: session,
                Targets: Targets
            });
        });
    }
    //}}


    describe('when no user logged in', function() {

        beforeEach(function() {
            session.username = undefined;
            target.locked = undefined;
            do_inject();
        });

        it("state is empty", function() {
            expect(scope.state).toBe("");
        });
    });



    describe('when logged in user and target locked by diff user', function() {

        beforeEach(function() {
            session.username = "user1";
            target.locked = ["user2"];
            do_inject();
        });

        it("state is queueable", function() {
            expect(scope.state).toBe("queueable");
        });
    });



    describe('when logged in user and target locked by same', function() {

        beforeEach(function() {
            session.username = "user1";
            target.locked = ["user1", "user2"];
            do_inject();
        });

        it("state is locked", function() {
            expect(scope.state).toBe("locked");
        });
    });


    describe('when logged in user and target locked by same (case-insensitive)', function() {

        beforeEach(function() {
            session.username = "User1";
            target.locked = ["uSer1", "user2"];
            do_inject();
        });

        it("state is locked", function() {
            expect(scope.state).toBe("locked");
        });
    });


    describe('when logged in user and target not locked (undefined)', function() {

        beforeEach(function() {
            session.username = "user1";
            target.locked = undefined;
            do_inject();
        });

        it("state is lockable", function() {
            expect(scope.state).toBe("lockable");
        });
    });


    describe('when logged in user and target not locked (empty)', function() {

        beforeEach(function() {
            session.username = "user1";
            target.locked = [];
            do_inject();
        });

        it("state is lockable", function() {
            expect(scope.state).toBe("lockable");
        });
    });

    describe('when logged in user and user in queue', function() {

        beforeEach(function() {
            session.username = "user1";
            target.locked = ["user2", "user3", "user1"];
            do_inject();
        });

        it("state is queued", function() {
            expect(scope.state).toBe("queued");
        });
    });

    describe('when session changes state changes', function() {
        beforeEach(function() {
            session.username = undefined;
            target.locked = undefined;
            do_inject();
        });

        it("changes state to lockable", function() {
            session.username = "user1";
            scope.$broadcast('session.updated');
            expect(scope.state).toBe("lockable");
        });
    })

    describe('when lock called', function() {
        beforeEach(function() {
            session.username = 'user1';
            target.locked = undefined;
            do_inject();
        });

        it("calls Targets.lock", function() {
            scope.lock();
            expect(Targets.lock).toHaveBeenCalledWith(target);
        });
    })

    describe('when unlock called', function() {
        beforeEach(function() {
            session.username = 'user1';
            target.locked = undefined;
            do_inject();
        });

        it("calls Targets.unlock", function() {
            scope.unlock();
            expect(Targets.unlock).toHaveBeenCalledWith(target);
        });
    })
});