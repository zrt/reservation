'use strict';

describe('User Service', function() {

    var service, $httpBackend, $rootScope;

    beforeEach(function() {

        angular.mock.module('myApp.users');

        inject(function(Users, $injector) {
            service = Users;
            $httpBackend = $injector.get('$httpBackend');
            $rootScope = $injector.get('$rootScope');
        });
    });

    afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });


    it('calls GET /users/username', function() {
        $httpBackend.expectGET('api/index.php/users/username').respond(200, {});
        service.get('username');
        $httpBackend.flush();
    });

    it('returns promise for get', function() {
        $httpBackend.expectGET().respond(200, {data: 1});
        var get = service.get('username').then(function(userdata) {
            expect(userdata).toEqual({data: 1});
        });
        $httpBackend.flush();
    });

    it('calls PUT on /users/username with data', function() {
        $httpBackend.expectPUT('api/index.php/users/username', {email: 'to-set'}).respond(200, {});
        service.put('username', { email: 'to-set' });
        $httpBackend.flush();
    });

    it('returns promise for put', function() {
        $httpBackend.expectPUT().respond(200);
        service.put('username', {}).then(function() {});
        $httpBackend.flush();
    });
});