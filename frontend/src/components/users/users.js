'use strict';

var module = angular.module('myApp.users', []);

module.service('Users', function($http, $q) {

    function get(name) {
        return $http({
                method: 'GET',
                url: 'api/index.php/users/' + name
            }).then(function(result) {
                return result.data;
            });
    }

    function put(name, data) {
        return $http({
                method: 'PUT',
                url: 'api/index.php/users/' + name,
                data: data
            });
    }

    return {
        get : get,
        put : put
    };
});